package com.wdl.view;

import java.io.File;
import java.security.NoSuchAlgorithmException;

import com.wdl.asynctask.AsyncTask4ModifyUserInfo;
import com.wdl.myclass.Constant;
import com.wdl.myclass.FileOperation;
import com.wdl.myclass.MD5;
import com.wdl.myclass.SqliteUtil;
import com.wdl.myclass.SystemSet;
import com.wdl.myclass.UsualTools;
import com.wdl.wandianle.R;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SetActivity extends Activity {
	private ActionBar mActionBar;
	private Button btnBack;
	private Button btnMpwd;
	private Button btnLogout;
	private Button btnHelp;
	private Button btnAbout;
	private Button btnMpwdSubmit;
	public  ImageView ivAvatar;
	public  TextView tvNickName;
	public  TextView tvUserName;
	private EditText etMpwdOPwd;
	private EditText etMpwdNPwd;
	private EditText etMpwdCPwd;
	public  ProgressDialog mProgressDialog;
	private LinearLayout lvCover;
	private LinearLayout lvMpwd;
	
	private File mFile;
	private Cursor mCursor;
	private SqliteUtil mSqliteUtil;
	private Intent mIntent;
	private Animation mAnimation;
	private SystemSet mSystemSet;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set);
		init();
	}
	
	private void init(){
		mActionBar = this.getActionBar();
		mActionBar.hide();
		
		btnBack = (Button) this.findViewById(R.id.back_set);
		btnMpwd = (Button) this.findViewById(R.id.btn_set_mpwd);
		btnLogout = (Button) this.findViewById(R.id.btn_set_logout);
		btnHelp = (Button) this.findViewById(R.id.btn_set_help);
		btnAbout =(Button) this.findViewById(R.id.btn_set_about);
		btnMpwdSubmit = (Button) this.findViewById(R.id.btn_mpwd_submit);
		ivAvatar = (ImageView) this.findViewById(R.id.set_avatar);
		tvNickName = (TextView) this.findViewById(R.id.tv_set_nickname);
		tvUserName = (TextView) this.findViewById(R.id.tv_set_username);
		etMpwdOPwd = (EditText) this.findViewById(R.id.mpwd_oldpwd);
		etMpwdNPwd = (EditText) this.findViewById(R.id.mpwd_npwd);
		etMpwdCPwd = (EditText) this.findViewById(R.id.mpwd_pwd_check);
		
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
		lvMpwd = (LinearLayout) this.findViewById(R.id.lv_mpwd);
		lvCover = (LinearLayout) this.findViewById(R.id.set_cover);
		
		btnBack.setOnClickListener(new BtnOnClickListener());
		btnMpwd.setOnClickListener(new BtnOnClickListener());
		btnLogout.setOnClickListener(new BtnOnClickListener());
		btnHelp.setOnClickListener(new BtnOnClickListener());
		btnAbout.setOnClickListener(new BtnOnClickListener());
		btnMpwdSubmit.setOnClickListener(new BtnOnClickListener());
		
		mSqliteUtil = new SqliteUtil(this, null);
		mCursor = mSqliteUtil.query(Constant.TABLE_USER, "*", Constant.ID + "=" + Constant.TABLE_ID_KEY, "", -1, -1);
		if (mCursor.getCount() == 0) {
			Toast.makeText(this, getResources().getString(R.string.msg_db_read_error), Toast.LENGTH_SHORT).show();
			return;
		}
		mCursor.moveToFirst();
		tvNickName.setText(mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NICKNAME)));
		tvUserName.setText(tvUserName.getText().toString() + " : " + mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NAME)));
		
		mFile = new File(FileOperation.getPackagePath(this, "").get(0) + Constant.USER_IMG_PATH + Constant.USER_AVATAR_NAME);
		if (mFile.exists()) {
			ivAvatar.setImageBitmap(UsualTools.toRoundCorner(UsualTools.readPic(FileOperation.getPackagePath(this, "").get(0) + Constant.USER_IMG_PATH + Constant.USER_AVATAR_NAME), 500));
		}else {
			Toast.makeText(SetActivity.this, getResources().getString(R.string.msg_no_avatar), Toast.LENGTH_SHORT).show();
		}
		mCursor.close();
	}
	
	private class BtnOnClickListener implements OnClickListener{
		
		@Override
		public void onClick(View v){
			switch (Integer.parseInt(v.getTag().toString())) {
			case 0://返回
				finish();
				break;

			case 1://修改密码
				lvCover.setVisibility(View.VISIBLE);
				lvMpwd.setVisibility(View.VISIBLE);
				mAnimation = AnimationUtils.loadAnimation(SetActivity.this, R.anim.common_anim);
				mAnimation.setAnimationListener(new OnAnimationListener());
				lvMpwd.setAnimation(mAnimation);
				btnEnableOrDisable(false);
				break;

			case 2://注销
				setResult(2);
				finish();
				break;

			case 3://帮助
				mIntent = new Intent(SetActivity.this, HelpActivity.class);
				startActivity(mIntent);
				break;

			case 4://关于
				mIntent = new Intent(SetActivity.this, AboutActivity.class);
				startActivity(mIntent);
				break;
				
			case 5://modify password submit
				if (etMpwdOPwd.getText().toString().equals("") || etMpwdNPwd.getText().toString().equals("") || etMpwdCPwd.getText().equals("")) {
					Toast.makeText(SetActivity.this, getResources().getString(R.string.msg_input_not_null), Toast.LENGTH_SHORT).show();
					return;
				}
				if (!(etMpwdNPwd.getText().toString().equals(etMpwdCPwd.getText().toString()))) {
					Toast.makeText(SetActivity.this, getResources().getString(R.string.msg_pwd_not_match), Toast.LENGTH_SHORT).show();
					return;
				}
				try {
					AsyncTask4ModifyUserInfo mAsyncTask4ModifyUserInfo = new AsyncTask4ModifyUserInfo(4, MD5.getMD5(etMpwdCPwd.getText().toString()), SetActivity.this);
					mAsyncTask4ModifyUserInfo.execute(MD5.getMD5(etMpwdOPwd.getText().toString()));
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				lvMpwd.setVisibility(View.GONE);
				lvCover.setVisibility(View.GONE);
				mSystemSet.showOrHideInputMethodManager();
				etMpwdCPwd.setText("");
				etMpwdNPwd.setText("");
				etMpwdOPwd.setText("");
				btnEnableOrDisable(true);
				break;

			default:
				break;
			}
		}
	}

	public void gotoUserCard(View v){
		mIntent = new Intent(SetActivity.this, UserCardActivity.class);
		startActivity(mIntent);
	}
	
	private class OnAnimationListener implements AnimationListener{

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			// TODO Auto-generated method stub
			etMpwdOPwd.requestFocus();
			mSystemSet = new SystemSet(SetActivity.this);
			mSystemSet.showOrHideInputMethodManager();
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
		if (keyCode == event.KEYCODE_BACK && lvCover.getVisibility() == View.VISIBLE) {
			lvMpwd.setVisibility(View.GONE);
			lvCover.setVisibility(View.GONE);
			btnEnableOrDisable(true);
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	private void btnEnableOrDisable(boolean flag){
		btnAbout.setClickable(flag);
		btnHelp.setClickable(flag);
		btnLogout.setClickable(flag);
		btnMpwd.setClickable(flag);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		if (mFile.exists()) {
			ivAvatar.setImageBitmap(UsualTools.toRoundCorner(UsualTools.readPic(FileOperation.getPackagePath(this, "").get(0) + Constant.USER_IMG_PATH + Constant.USER_AVATAR_NAME), 500));
		}
	}
}
