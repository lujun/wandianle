package com.wdl.asynctask;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.wdl.myclass.Constant;
import com.wdl.myclass.HttpOperation;
import com.wdl.wandianle.R;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class AsyncTask4SendSuggestion extends AsyncTask<String, Integer, Boolean> {
	private Context mContext;
	private HttpOperation mHttpOperation;
	private ArrayList<NameValuePair> mArrayList;
	private JSONObject mJsonObject;

	public AsyncTask4SendSuggestion(Context context) {
		this.mContext = context;
		this.mHttpOperation = new HttpOperation(1, Constant.SERVER_SUG_URL, "", null);
		this.mArrayList = new ArrayList<NameValuePair>();
	}
	
	@Override
	protected Boolean doInBackground(String... params){
		try {
			mArrayList.add(new BasicNameValuePair("username", params[0]));
			mArrayList.add(new BasicNameValuePair("password", params[1]));
			mArrayList.add(new BasicNameValuePair("idea", params[2]));
			mHttpOperation.setHttpRequest(mArrayList);
			while(!mHttpOperation.mDoneFlag){}
			mJsonObject = new JSONObject(mHttpOperation.mResult);
			if (mJsonObject.getString("res") == null || mJsonObject.getString("res").equals("") || mJsonObject.getString("res").equals("2220")) {
				return false;
			}else if(mJsonObject.getString("res").equals("2221")){
				return true;
			}else {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	protected void onPostExecute(Boolean result){
		if (result) {
			Toast.makeText(mContext, mContext.getResources().getString(R.string.msg_send_succeed), Toast.LENGTH_SHORT).show();
		}else {
			Toast.makeText(mContext, mContext.getResources().getString(R.string.msg_send_error), Toast.LENGTH_SHORT).show();
		}
	}
}
