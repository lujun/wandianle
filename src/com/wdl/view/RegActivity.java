package com.wdl.view;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.wdl.myclass.Constant;
import com.wdl.myclass.HttpOperation;
import com.wdl.myclass.MD5;
import com.wdl.myclass.NetWorkInfo;
import com.wdl.myclass.SqliteUtil;
import com.wdl.myclass.SystemSet;
import com.wdl.wandianle.MainActivity;
import com.wdl.wandianle.R;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class RegActivity extends Activity {
	private ActionBar mActionBar;
	private Button btnRegSubmit;
	private EditText etRegUserName;
	private EditText etRegPassword;
	private EditText etRegPasswordCheck;
	private ProgressDialog mProgressDialog;
	private LinearLayout llReg;
	
	private HttpOperation mHttpOperation;
	private ArrayList<NameValuePair> mList;
	private ArrayList<Object> mArrayList;
	private JSONObject mJsonObject;
	private Intent mIntent;
	private Animation mAnimation;
	private SystemSet mSystemSet;
	
	private SqliteUtil mSqliteUtil;
	private Cursor mCursor;
	
	private Handler mHandler = new Handler(){
		
		@Override
		public void handleMessage(Message msg){
			super.handleMessage(msg);
			switch (msg.what) {
			case 0://http
				dealResult(mHttpOperation.mResult);
				break;

			default:
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reg);
		init();
	}

	private void init(){
		mActionBar = this.getActionBar();
		mActionBar.hide();
		
		etRegUserName = (EditText) this.findViewById(R.id.reg_username);
		etRegPassword = (EditText) this.findViewById(R.id.reg_password);
		etRegPasswordCheck = (EditText) this.findViewById(R.id.reg_password_check);
		btnRegSubmit = (Button) this.findViewById(R.id.btn_reg_submit);
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
		llReg = (LinearLayout) this.findViewById(R.id.linearlayout_reg);
		
		mList = new ArrayList<NameValuePair>();
		mArrayList = new ArrayList<Object>();
		mSqliteUtil = new SqliteUtil(this, null);
		
		btnRegSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!NetWorkInfo.isNetWorkConnected(RegActivity.this)) {
					Toast.makeText(RegActivity.this, getResources().getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
					return;
				}
				if (!etRegPassword.getText().toString().equals(etRegPasswordCheck.getText().toString()) || etRegUserName.getText().toString().equals("") || etRegPassword.getText().toString().equals("") || etRegPasswordCheck.getText().toString().equals("")) {
					Toast.makeText(RegActivity.this, getResources().getString(R.string.msg_passwprd_check_error), Toast.LENGTH_SHORT).show();
					return;
				}
				mProgressDialog.show();
				try {
					mHttpOperation = new HttpOperation(1, Constant.SERVER_REG_URL, "", mHandler);
					mList.add(new BasicNameValuePair("username", etRegUserName.getText().toString()));
					mList.add(new BasicNameValuePair("password", MD5.getMD5(etRegPasswordCheck.getText().toString())));
					mHttpOperation.setHttpRequest(mList);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		mAnimation = AnimationUtils.loadAnimation(RegActivity.this, R.anim.loginandreg_anim);
		mAnimation.setAnimationListener(new OnAnimationListener());
		llReg.setAnimation(mAnimation);
	}
	
	private void dealResult(String result){
		mProgressDialog.dismiss();
		try {
			mJsonObject = new JSONObject(result);
			if (mJsonObject.getString(Constant.JSON_RESULT_HEAD) == null || mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("")) {
				Toast.makeText(RegActivity.this, getResources().getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
				return;
			}
			if (mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("0000")) {
				Toast.makeText(RegActivity.this, getResources().getString(R.string.r0000), Toast.LENGTH_SHORT).show();
				return;
			}
			if (mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("0002")) {
				Toast.makeText(RegActivity.this, getResources().getString(R.string.r0002), Toast.LENGTH_SHORT).show();
				return;
			}
			if (mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("0001")) {
				try {
					mArrayList.add(Constant.TABLE_USER_NAME);
					mArrayList.add(etRegUserName.getText().toString());
					mArrayList.add(Constant.TABLE_USER_PASSWORD);
					mArrayList.add(MD5.getMD5(etRegPasswordCheck.getText().toString()));
					mArrayList.add(Constant.TABLE_USER_NICKNAME);
					mArrayList.add("");
					mArrayList.add(Constant.TABLE_USER_SINCON);
					mArrayList.add("");
					mArrayList.add(Constant.TABLE_USER_SEX);
					mArrayList.add("");
					mArrayList.add(Constant.TABLE_USER_HEADURL);
					mArrayList.add("");
					mArrayList.add(Constant.TABLE_USER_PHONE);
					mArrayList.add("");
					mArrayList.add(Constant.TABLE_USER_SCORE);
					mArrayList.add("");
					mArrayList.add(Constant.TABLE_USER_CANSHARE);
					mArrayList.add("");
					
					mSqliteUtil = new SqliteUtil(RegActivity.this, null);
					mCursor = mSqliteUtil.query(Constant.TABLE_USER, "*", Constant.ID + "=" + Constant.TABLE_ID_KEY, "", -1, -1);
					if (mCursor.getCount() == 0) {
						mCursor.close();
						if (!mSqliteUtil.insert(Constant.TABLE_USER, mArrayList)) {
							return;
						};
					}else {
						mCursor.close();
						if (!mSqliteUtil.update(Constant.TABLE_USER, mArrayList, Constant.ID + "=" + Constant.TABLE_ID_KEY)) {
							return;
						}
					}
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				mIntent = new Intent(RegActivity.this, MainActivity.class);
				startActivity(mIntent);
				finish();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	private class OnAnimationListener implements AnimationListener{

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			// TODO Auto-generated method stub
			etRegUserName.requestFocus();
			mSystemSet = new SystemSet(RegActivity.this);
			mSystemSet.showOrHideInputMethodManager();
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
		}
	}
}
