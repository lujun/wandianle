package com.wdl.asynctask;

import java.io.File;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.wdl.myclass.Constant;
import com.wdl.myclass.FileOperation;
import com.wdl.myclass.HttpOperation;
import com.wdl.myclass.SqliteUtil;
import com.wdl.myclass.UsualTools;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;

public class AsyncTask4UpdateUserInfo extends AsyncTask<String, Integer, Boolean> {
	private Context mContext;
	private Bitmap mBitmap;
	private SqliteUtil mSqliteUtil;
	private ArrayList<Object> mArrayList;
	private ArrayList<NameValuePair> mArrayList4HTTP;
	private HttpOperation mHttpOperation;
	private JSONObject mJsonObject;
	private Cursor mCursor;

	public AsyncTask4UpdateUserInfo(Context context) {
		// TODO Auto-generated constructor stub
		mContext = context;
		this.mSqliteUtil = new SqliteUtil(context, null);
		this.mArrayList = new ArrayList<Object>();
		this.mArrayList4HTTP = new ArrayList<NameValuePair>();
	}

	@Override
	protected void onPreExecute() {
	}

	@Override
	protected Boolean doInBackground(String... params) {
		try {
			File file = new File(FileOperation.getPackagePath(mContext, "").get(0) + Constant.USER_IMG_PATH + Constant.USER_AVATAR_NAME);
			if (file.exists()) {
				file.delete();
			}
			mBitmap = UsualTools.downloadBitmap(params[0]);
			if (mBitmap == null) {
				return false;
			}
			if (!UsualTools.saveBitmap(mContext, 0, Constant.USER_IMG_PATH,  mBitmap, Constant.USER_AVATAR_NAME)) {
				return false;
			}
			mCursor = mSqliteUtil.query(Constant.TABLE_USER, "*", Constant.ID + "=" + Constant.TABLE_ID_KEY, "", -1, -1);
			mCursor.moveToFirst();
			mArrayList4HTTP.add(new BasicNameValuePair("username",mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NAME))));
			mArrayList4HTTP.add(new BasicNameValuePair("password", mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_PASSWORD))));
			mHttpOperation = new HttpOperation(1,Constant.SERVER_USER_INFO_URL, "", null);
			mHttpOperation.setHttpRequest(mArrayList4HTTP);
			while (!mHttpOperation.mDoneFlag) {}
			mJsonObject = new JSONObject(mHttpOperation.mResult);
			if (mJsonObject.getString("username") == null || mJsonObject.getString("username").equals("")) {
				return false;
			}
			mArrayList.add(Constant.TABLE_USER_NICKNAME);
			mArrayList.add(mJsonObject.getString("nickname"));
			mArrayList.add(Constant.TABLE_USER_SINCON);
			mArrayList.add(mJsonObject.getString("sincon"));
			mArrayList.add(Constant.TABLE_USER_SEX);
			mArrayList.add(mJsonObject.getString("sex"));
			mArrayList.add(Constant.TABLE_USER_HEADURL);
			mArrayList.add(mJsonObject.getString("headurl"));
			mArrayList.add(Constant.TABLE_USER_PHONE);
			mArrayList.add(mJsonObject.getString("phone"));
			mArrayList.add(Constant.TABLE_USER_SCORE);
			mArrayList.add(mJsonObject.getString("score"));
			mArrayList.add(Constant.TABLE_USER_CANSHARE);
			mArrayList.add(mJsonObject.getString("canshare"));
			if (!mSqliteUtil.update(Constant.TABLE_USER, mArrayList, Constant.ID + "=" + Constant.TABLE_ID_KEY)) {
				return false;
			}
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	@Override
	protected void onProgressUpdate(Integer... params) {

	}

	@Override
	protected void onPostExecute(Boolean result) {
	}

	@Override
	protected void onCancelled() {

	}
}
