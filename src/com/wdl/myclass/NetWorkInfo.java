package com.wdl.myclass;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * 检测当前网络状态类
 * 
 * @author lujun
 * @since 2014-08-04
 *
 */
public class NetWorkInfo {
	/**
	 * ConnectivityManager:
	 * 1、监听手机状态(GPRS,WIFI...)
	 * 2、手机状态发生改变时，发送广播
	 * 3、当一个网络连接失败时进行故障切换
	 * 4、为应用提供可以获取可用网络的高精度和粗糙状态
	 */
	
	/**
	 * 判断是否连接到网络
	 * 
	 *  @param context 判断时程序所在的上下文
	 */
	public static boolean isNetWorkConnected(Context context){
		if (context != null) {
			ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netWorkInfo = connectivityManager.getActiveNetworkInfo();
			if (netWorkInfo != null) {
				return netWorkInfo.isAvailable();
			}
		}
		return false;
	}
	
	/**
	 * 判断WIFI是否可用
	 * 
	 * @param context 判断时程序所在的上下文
	 */
	public static boolean isWifiConnected(Context context){
		if (context != null) {
			ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo wifiNetWorkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			if (wifiNetWorkInfo != null) {
				return wifiNetWorkInfo.isAvailable();
			}
		}
		return false;
	}
	
	/**
	 * 判断移动数据连接是否可用
	 * 
	 * @param context 判断时程序所在的上下文
	 */
	public static boolean isMobileConnected(Context context){
		if (context != null) {
			ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mobileNetWorkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			if (mobileNetWorkInfo != null) {
				return mobileNetWorkInfo.isAvailable();
			}
		}
		return false;
	}
	
	/**
	 * 获取当前网络连接的类型
	 * 
	 * @param context 判断时程序所在的上下文
	 */
	public static int getConnectedType(Context context){
		if (context != null) {
			ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netWorkInfo = connectivityManager.getActiveNetworkInfo();
			if (netWorkInfo != null && netWorkInfo.isAvailable()) {
				return netWorkInfo.getType();
			}
		}
		return -1;
	}
}
