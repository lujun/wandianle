package com.wdl.view;

import java.io.File;

import org.apache.http.message.BasicNameValuePair;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.wdl.asynctask.AsyncTask4ModifyUserInfo;
import com.wdl.myclass.Constant;
import com.wdl.myclass.FileOperation;
import com.wdl.myclass.ImageTools;
import com.wdl.myclass.SqliteUtil;
import com.wdl.myclass.SystemSet;
import com.wdl.myclass.UsualTools;
import com.wdl.wandianle.R;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class UserCardActivity extends Activity {
	private ActionBar mActionBar;
	private Button btnBack;
	private ImageView ivUserAvatar;
	private TextView tvUserName;
	private TextView tvUserNickName;
	private TextView tvUserSex;
	private TextView tvUserSign;
	private TextView tvUserScore;
	private LinearLayout lvCover;
	private LinearLayout lvCover2;
	private LinearLayout lvMNickName;
	private LinearLayout lvMSign;
	private LinearLayout lvUserSex;
	private Button btnMSign;
	private Button btnMNickName;
	private EditText etNickName;
	private EditText etSign;
	private ProgressDialog mProgressDialog;
	
	private SqliteUtil mSqliteUtil;
	private Cursor mCursor;
	private Intent mIntent;
	private Animation mAnimation;
	private SystemSet mSystemSet;
	private AsyncTask4ModifyUserInfo mAsyncTask4ModifyUserInfo;
	
	private boolean isClickable;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_usercard);
		init();
	}
	
	private void init(){
		isClickable = true;//是否可点击
		mActionBar = this.getActionBar();
		mActionBar.hide();
		btnBack = (Button) this.findViewById(R.id.back_usercard);
		ivUserAvatar = (ImageView) this.findViewById(R.id.usercard_avatar);
		tvUserName = (TextView) this.findViewById(R.id.usercard_username);
		tvUserNickName = (TextView) this.findViewById(R.id.usercard_nickname);
		tvUserSex = (TextView) this.findViewById(R.id.usercard_sex);
		tvUserSign = (TextView) this.findViewById(R.id.usercard_sign);
		tvUserScore = (TextView) this.findViewById(R.id.usercard_score);
		btnMNickName = (Button) this.findViewById(R.id.btn_nickname_submit);
		btnMSign = (Button) this.findViewById(R.id.btn_sign_submit);
		etSign = (EditText) this.findViewById(R.id.msign_text);
		etNickName = (EditText) this.findViewById(R.id.mnickname_text);
		lvMNickName = (LinearLayout) this.findViewById(R.id.usercard_mnickname);
		lvMSign = (LinearLayout) this.findViewById(R.id.usercard_msign);
		lvCover = (LinearLayout) this.findViewById(R.id.usercard_cover);
		lvCover2 = (LinearLayout) this.findViewById(R.id.usercard_cover2);
		lvUserSex = (LinearLayout) this.findViewById(R.id.lv_usercard_sex);
		mProgressDialog = new ProgressDialog(UserCardActivity.this);
		
		btnBack.setOnClickListener(new BtnClickListener());
		btnMNickName.setOnClickListener(new BtnClickListener());
		btnMSign.setOnClickListener(new BtnClickListener());
		registerForContextMenu(lvUserSex);
		mSystemSet = new SystemSet(UserCardActivity.this);
		mSqliteUtil = new SqliteUtil(UserCardActivity.this, null);
		mCursor = mSqliteUtil.query(Constant.TABLE_USER, "*", Constant.ID + "=" + Constant.TABLE_ID_KEY, "", -1, -1);

		File file = new File(FileOperation.getPackagePath(this, "").get(0) + Constant.USER_IMG_PATH + Constant.USER_AVATAR_NAME);
		if (!file.exists()) {
			Toast.makeText(UserCardActivity.this, getResources().getString(R.string.msg_no_avatar), Toast.LENGTH_SHORT).show();
		}else {
			ivUserAvatar.setImageBitmap(UsualTools.readPic(FileOperation.getPackagePath(this, "").get(0) + Constant.USER_IMG_PATH + Constant.USER_AVATAR_NAME));
		}
		if (mCursor.getCount() == 0) {
			Toast.makeText(UserCardActivity.this, getResources().getString(R.string.msg_db_read_error), Toast.LENGTH_SHORT).show();
			return;
		}
		mCursor.moveToFirst();
		tvUserName.setText(mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NAME)));
		tvUserNickName.setText(mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NICKNAME)));
		tvUserSex.setText(mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_SEX)));
		tvUserSign.setText(mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_SINCON)));
		tvUserScore.setText(mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_SCORE)));
		mCursor.close();
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo){
		menu.add(Menu.NONE, Menu.FIRST+1, 1, getResources().getString(R.string.menu_usercard_m));
		menu.add(Menu.NONE, Menu.FIRST+2, 2, getResources().getString(R.string.menu_usercard_w));
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case Menu.FIRST + 1://M
			tvUserSex.setText(getResources().getString(R.string.menu_usercard_m));
			mAsyncTask4ModifyUserInfo = new AsyncTask4ModifyUserInfo(3, getResources().getString(R.string.menu_usercard_m), UserCardActivity.this);
			mAsyncTask4ModifyUserInfo.execute("");
			break;

		case Menu.FIRST + 2://W
			tvUserSex.setText(getResources().getString(R.string.menu_usercard_w));
			mAsyncTask4ModifyUserInfo = new AsyncTask4ModifyUserInfo(3, getResources().getString(R.string.menu_usercard_w), UserCardActivity.this);
			mAsyncTask4ModifyUserInfo.execute("");
			break;

		default:
			break;
		}
		return true;
	}
	
	private class BtnClickListener implements OnClickListener{
		
		@Override
		public void onClick(View v){
			switch (Integer.parseInt(v.getTag().toString())) {
			case 0:
				finish();
				break;

			case 1://modify nickname
				isClickable  = true;
				tvUserNickName.setText(etNickName.getText().toString());
				mAsyncTask4ModifyUserInfo = new AsyncTask4ModifyUserInfo(1, etNickName.getText().toString(), UserCardActivity.this);
				mAsyncTask4ModifyUserInfo.execute("");
				break;
				
			case 2://modify signature
				isClickable  = true;
				tvUserSign.setText(etSign.getText().toString());
				mAsyncTask4ModifyUserInfo = new AsyncTask4ModifyUserInfo(2, etSign.getText().toString(), UserCardActivity.this);
				mAsyncTask4ModifyUserInfo.execute("");
				break;
				
			default:
				break;
			}
			lvCover.setVisibility(View.GONE);
			lvCover2.setVisibility(View.GONE);
			lvMNickName.setVisibility(View.GONE);
			lvMSign.setVisibility(View.GONE);
			mSystemSet.showOrHideInputMethodManager();
		}
	}
	
	public void modifyInfo(View v){
		if (!isClickable) {
			return;
		}
		switch (Integer.parseInt(v.getTag().toString())) {
		case 0:
			mIntent = new Intent(Intent.ACTION_PICK,Media.EXTERNAL_CONTENT_URI);// 调用系统图库
			startActivityForResult(mIntent, 0);
			break;
			
		case 1:
			mCursor = mSqliteUtil.query(Constant.TABLE_USER, "*", Constant.ID + "=" + Constant.TABLE_ID_KEY, "", -1, -1);
			if (mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				etNickName.setText(mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NICKNAME)));
			}
			mCursor.close();
			lvCover2.setVisibility(View.GONE);
			lvMSign.setVisibility(View.GONE);
			lvCover.setVisibility(View.VISIBLE);
			lvMNickName.setVisibility(View.VISIBLE);
			mAnimation = AnimationUtils.loadAnimation(UserCardActivity.this, R.anim.common_anim);
			mAnimation.setAnimationListener(new OnAnimationListener(0));
			lvMNickName.setAnimation(mAnimation);
			break;
			
		case 4:
			mCursor = mSqliteUtil.query(Constant.TABLE_USER, "*", Constant.ID + "=" + Constant.TABLE_ID_KEY, "", -1, -1);
			if (mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				etSign.setText(mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_SINCON)));
			}
			mCursor.close();
			lvCover.setVisibility(View.GONE);
			lvMNickName.setVisibility(View.GONE);
			lvCover2.setVisibility(View.VISIBLE);
			lvMSign.setVisibility(View.VISIBLE);
			mAnimation = AnimationUtils.loadAnimation(UserCardActivity.this, R.anim.common_anim);
			mAnimation.setAnimationListener(new OnAnimationListener(1));
			lvMSign.setAnimation(mAnimation);
			break;

		default:
			break;
		}
		isClickable = false;
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0 && resultCode == Activity.RESULT_OK) {//图库返回
			Uri uri = data.getData();
			mCursor = UserCardActivity.this.getContentResolver().query(uri, null, null, null, null);
			mCursor.moveToFirst();
//			String imgNo = mCursor.getString(0); // 图片编号
//			String imgPath = mCursor.getString(1); // 图片文件路径
//			String imgSize = mCursor.getString(2); // 图片大小
//			String imgName = mCursor.getString(3); // 图片文件名
			dealUploadAvatar(mCursor.getString(1), mCursor.getString(2));
			mCursor.close();
		}
	}
	
	/**
	 * 图片上传
	 * 
	 * @param path  图片路径
	 * @param size  图片大小
	 */
	private void dealUploadAvatar(String path, String size){
		if (Long.parseLong(size) > 10485760) {
			Toast.makeText(UserCardActivity.this, getResources().getString(R.string.msg_pic_size_outof_bound), Toast.LENGTH_SHORT).show();
			return;
		}
		if (!UsualTools.saveBitmap(UserCardActivity.this, 0, Constant.USER_IMG_PATH,  ImageTools.zoomBitmap(UsualTools.readPic(path), 120, 120), Constant.USER_AVATAR_NAME)) {
			return;
		}
		path = FileOperation.getPackagePath(UserCardActivity.this, "").get(0) + Constant.USER_IMG_PATH + Constant.USER_AVATAR_NAME;
		RequestParams requestParams = new RequestParams();
		requestParams.addBodyParameter(new BasicNameValuePair("username", tvUserName.getText().toString()));
		requestParams.addBodyParameter("file", new File(path));
		HttpUtils httpUtils = new HttpUtils();
		isClickable = true;
		httpUtils.send(HttpRequest.HttpMethod.POST, Constant.SERVER_UPLOAD_AVATAR, requestParams, new RequestCallBack<String>() {
			
			@Override
			public void onStart(){
				mProgressDialog.show();
			}
			
			@Override
			public void onLoading(long total, long current, boolean isUploading){
				if (isUploading) {
					mProgressDialog.setMessage(getResources().getString(R.string.please_wait) + "：" + current + "/" + total);
				}
			}
			
			@Override
			public void onSuccess(ResponseInfo<String> responseInfo){
				//TODO 及时更新头像未解决，下载为空
				/*Bitmap tmpBitmap = UsualTools.downloadBitmap(Constant.SERVER_USER_AVATAR_URL + tvUserName.getText().toString() + ".png");
				if (tmpBitmap != null) {Log.d("debug", "notnull");
					File file = new File(FileOperation.getPackagePath(UserCardActivity.this, "").get(0) + Constant.USER_IMG_PATH + Constant.USER_AVATAR_NAME);
					if (file.exists()) {
						file.delete();
					}
					if (UsualTools.saveBitmap(UserCardActivity.this, 0, Constant.USER_IMG_PATH,  tmpBitmap, Constant.USER_AVATAR_NAME)) {
						ivUserAvatar.setImageBitmap(tmpBitmap);
					}
				}*/
				//XXX 本地压缩解决
				ivUserAvatar.setImageBitmap(UsualTools.readPic(FileOperation.getPackagePath(UserCardActivity.this, "").get(0) + Constant.USER_IMG_PATH + Constant.USER_AVATAR_NAME));
				mProgressDialog.setMessage(getResources().getString(R.string.msg_upload_success));
				mProgressDialog.dismiss();
			}
			
			@Override
			public void onFailure(HttpException error, String msg){
				mProgressDialog.setMessage(getResources().getString(R.string.msg_upload_failed));
				mProgressDialog.dismiss();
			}
		});
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
		if (keyCode == event.KEYCODE_BACK && (lvCover.getVisibility() == View.VISIBLE || lvCover2.getVisibility() == View.VISIBLE)) {
			lvCover2.setVisibility(View.GONE);
			lvMNickName.setVisibility(View.GONE);
			lvCover.setVisibility(View.GONE);
			lvMSign.setVisibility(View.GONE);
			isClickable = true;
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	private class OnAnimationListener implements AnimationListener{
		private int flag;
		
		public OnAnimationListener(int arg0){
			this.flag = arg0;
		}

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			// TODO Auto-generated method stub
			if (flag == 0) {
				etNickName.requestFocus();
			}else if (flag == 1) {
				etSign.requestFocus();
			}
			mSystemSet.showOrHideInputMethodManager();
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
		}
	}
}
