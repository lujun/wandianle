package com.wdl.asynctask;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.wdl.myclass.Constant;
import com.wdl.myclass.HttpOperation;
import com.wdl.wandianle.R;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class AsyncTask4UDShareCon extends AsyncTask<String, Integer, Boolean> {
	private Context mContext;
	private HttpOperation mHttpOperation;
	private ArrayList<NameValuePair> mArrayList;
	private JSONObject mJsonObject;
	private boolean mDoneFlag;
	private int udFlag;

	public AsyncTask4UDShareCon(Context context, int flag) {
		this.mContext = context;
		this.mHttpOperation = new HttpOperation(1, Constant.SERVER_UD_URL, "", null);
		this.udFlag = flag;
		this.mArrayList = new ArrayList<NameValuePair>();
		this.mDoneFlag = false;
	}

	@Override
	protected Boolean doInBackground(String... params) {
		try {
			mArrayList.add(new BasicNameValuePair("username", params[0]));
			mArrayList.add(new BasicNameValuePair("id", params[1]));
			mArrayList.add(new BasicNameValuePair("flag", (udFlag == 1)? "1":"2" ));
			mHttpOperation.setHttpRequest(mArrayList);
			while(!mHttpOperation.mDoneFlag){}
			mJsonObject = new JSONObject(mHttpOperation.mResult);
			if (mJsonObject.getString(Constant.JSON_RESULT_HEAD) == null || mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("")) {
				return false;
			}else if(mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("4442")){
				mDoneFlag = true;
				return false;
			}else if (mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("4441")) {
				return true;
			}
			return false;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}

	@Override
	protected void onPostExecute(Boolean result) {
		if (result) {
			Toast.makeText(mContext, mContext.getResources().getString(R.string.msg_operate_success), Toast.LENGTH_SHORT).show();
		}else{
			if (mDoneFlag) {
				if (udFlag == 1) {
					Toast.makeText(mContext, mContext.getResources().getString(R.string.msg_have_up), Toast.LENGTH_SHORT).show();
				}else if (udFlag == 2) {
					Toast.makeText(mContext, mContext.getResources().getString(R.string.msg_have_down), Toast.LENGTH_SHORT).show();
				}else {
					Toast.makeText(mContext, mContext.getResources().getString(R.string.msg_operate_failed), Toast.LENGTH_SHORT).show();
				}
			}else {
				Toast.makeText(mContext, mContext.getResources().getString(R.string.msg_operate_failed), Toast.LENGTH_SHORT).show();
			}
		}
	}

}
