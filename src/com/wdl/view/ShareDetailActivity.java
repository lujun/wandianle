package com.wdl.view;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.loopj.android.image.SmartImageView;
import com.wdl.asynctask.AsyncTask4UDShareCon;
import com.wdl.myclass.Constant;
import com.wdl.wandianle.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

public class ShareDetailActivity extends Activity {
	private Button btnBack;
	private SmartImageView sivAvatar;
	private TextView tvNickName;
	private TextView tvSTime;
	private TextView tvConWords;
	private TextView tvTrainNo;
	private TextView tvTrainTime;
	private TextView tvTrainLocation;
	private TextView tvAgree;
	private TextView tvDisAgree;
	private WebView wvMap;
	private ActionBar mActionBar;
	
	private Intent mIntent;
	private Bundle mBundle;
	private SimpleDateFormat mDateFormat;
	private AsyncTask4UDShareCon mAsyncTask4UDShareCon;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_share_detail);
		init();
	}

	private void init(){
		mActionBar = this.getActionBar();
		mActionBar.hide();
		mIntent = this.getIntent();
		mBundle = mIntent.getExtras();
		mDateFormat = new SimpleDateFormat("MM-dd HH:mm");
		
		btnBack = (Button) this.findViewById(R.id.back_share_detail);
		sivAvatar = (SmartImageView) this.findViewById(R.id.sd_avatar);
		tvNickName = (TextView) this.findViewById(R.id.sd_nickname);
		tvSTime = (TextView) this.findViewById(R.id.sd_stime);
		tvConWords = (TextView) this.findViewById(R.id.sd_conwords);
		tvTrainNo = (TextView) this.findViewById(R.id.sd_train_no);
		tvTrainTime = (TextView) this.findViewById(R.id.sd_train_time);
		tvTrainLocation = (TextView) this.findViewById(R.id.sd_train_location);
		tvAgree = (TextView) this.findViewById(R.id.sd_agree);
		tvDisAgree = (TextView) this.findViewById(R.id.sd_disagree);
		wvMap = (WebView) this.findViewById(R.id.sd_map_wv);
		
		btnBack.setOnClickListener(new BtnOnClickListener());
		tvAgree.setOnClickListener(new BtnOnClickListener());
		tvDisAgree.setOnClickListener(new BtnOnClickListener());
		
		sivAvatar.setImageUrl(Constant.SERVER_USER_AVATAR_URL + mBundle.getString("username").toString() + ".png");
		tvNickName.setText(mBundle.getString("username"));
		tvSTime.setText(getResources().getString(R.string.hint_shareat) + mDateFormat.format(new Date(Long.parseLong(mBundle.getString("time"))*1000)));
		tvConWords.setText(getResources().getString(R.string.hint_sharesay) + mBundle.getString("context"));
		tvTrainNo.setText(mBundle.getString("trainno").toUpperCase());
		tvTrainTime.setText(mBundle.getString("latetime"));
		tvTrainLocation.setText(mBundle.getString("trainlocation"));
		tvAgree.setText(getResources().getString(R.string.agree) + mBundle.getString("up"));
		tvDisAgree.setText(getResources().getString(R.string.disagree) + mBundle.getString("down"));
		
		initWebView(wvMap);
		wvMap.loadUrl(Constant.SERVER_MAP_URL + "?longitude=" + mBundle.getString("longitude") + "&latitude=" + mBundle.getString("latitude"));
	}
	
	private void initWebView(WebView webview){
		webview.setVerticalScrollBarEnabled(false);                                         //滚动条垂直不显示
		webview.setHorizontalScrollBarEnabled(false);                                       //滚动条水平不显示
		webview.getSettings().setJavaScriptEnabled(true);                                   //允许javascript运行
		webview.getSettings().setUseWideViewPort(false);                                    //将图片调整到合适的webview的大小（FALSE不调整）
		webview.getSettings().setSupportZoom(false);                                        //设置是否支持缩放
		webview.getSettings().setDomStorageEnabled(true);                                   //开启DOM storage API功能
		webview.getSettings().setDatabaseEnabled(true);                                     //开启DATABASE storage API功能
		webview.getSettings().setAppCacheEnabled(true);                                     //开启APP缓存功能
		webview.getSettings().setAllowFileAccess(true);                                     //设置可以访问文件 
		webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);               //支持通过JS打开新窗口 
		webview.getSettings().setLoadsImagesAutomatically(true);                            //支持自动加载图片
		webview.getSettings().setUseWideViewPort(true);                                     //设置webview使用推荐窗口
		webview.getSettings().setLoadWithOverviewMode(true);                                //设置webview加载页面的模式
	}
	
	private class BtnOnClickListener implements OnClickListener{
			
			@Override
			public void onClick(View v){
				switch (Integer.parseInt(v.getTag().toString())) {
				case 0:
					finish();
					break;
					
				case 1://赞
					mAsyncTask4UDShareCon = new AsyncTask4UDShareCon(ShareDetailActivity.this, 1);
					mAsyncTask4UDShareCon.execute(new String[]{mBundle.getString("username"), mBundle.getString("id")});
					tvAgree.setText(tvAgree.getText().toString().substring(0, 1) + (Integer.parseInt(tvAgree.getText().toString().substring(1, tvAgree.getText().toString().length())) + 1));
					tvAgree.setClickable(false);
					break;
	
				default://踩
					mAsyncTask4UDShareCon = new AsyncTask4UDShareCon(ShareDetailActivity.this, 2);
					mAsyncTask4UDShareCon.execute(new String[]{mBundle.getString("username"), mBundle.getString("id")});
					tvDisAgree.setText(tvDisAgree.getText().toString().substring(0, 1) + (Integer.parseInt(tvDisAgree.getText().toString().substring(1, tvDisAgree.getText().toString().length())) + 1));
					tvDisAgree.setClickable(false);
					break;
				}
			}
		}
}
