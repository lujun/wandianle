package com.wdl.myclass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import android.os.Handler;

/**
 * HTTP操作类，功能包括发送HTTP请求（get方式和post方式），可以选择是否返回请求值
 * 
 * @author lujun
 * @since  2014-08-06
 *
 */
public class HttpOperation {
	/** 类成员变量*/
	private static final int IS_DONE = 0;//处理完成标志
	private int mHttpType;               //请求类型 0：GET 1：POST
	private String mHostUrl;             //请求的主机地址，如：http://www.lib.hnist.cn
	private String mHttpArgs;            //请求参数，如："username=lujun&cid=12345678"。没有需要为空""
	private String mHttpUrl;             //请求URL
	private HttpGet mHttpGet;            //GET请求对象
	private HttpPost mHttpPost;          //POST请求对象
	private HttpClient mHttpClient;      //http客户端请求对象
	private Handler mHandler;            //UI消息处理体
	public  String mResult;              //返回结果
	public boolean mDoneFlag;            //请求完成标识

	/** 
	 * 构造函数
	 * 
	 * @param HttpType 请求类型 0：GET 1：POST
	 * @param HostUrl  请求的主机地址，如：http://www.lib.hnist.cn
	 * @param HttpArgs 请求参数，如："username=lujun&cid=12345678"。请求方式为POST和没有参数时需要为空字符串""
	 * @param Handler  消息处理示例，若需要返回消息，则需要传入一个Handler处理示例。不需要则为null。返回的消息对象中what为0
	 */
	public HttpOperation(int HttpType, String HostUrl, String HttpArgs, Handler Handler) {
		// TODO Auto-generated constructor stub
		this.mHttpType = HttpType;
		this.mHostUrl  = HostUrl;
		this.mHttpArgs = HttpArgs;
		this.mHttpUrl  = (this.mHttpArgs.equals("")) ? (this.mHostUrl) : (this.mHostUrl + "?" + this.mHttpArgs);
		this.mHandler  = (Handler==null) ? null:Handler;
		this.mResult   = "";
		this.mDoneFlag = false;
	}
	
	/**
	 * 设置HTTP请求并发送请求方法
	 * 
	 * @param  postValue    post请求所需的数据，get请求此为null
	 * 
	 * @return 成功返回true，若需要返回的数据，可在返回成功消息处理中截获当前HttpOperation对象的成员变量mResult(String类型)
	 */
	public boolean setHttpRequest(ArrayList<NameValuePair> postValue){
		switch (mHttpType) {
		case 0:
			if (!sendGetRequest()) {
				return false;
			}
			break;
			
		case 1:
			if (!sendPostRequest(postValue)) {
				return false;
			}
			break;

		default:
			break;
		}
		return true;
	}
	
	/**
	 * 发送GET请求方法
	 * 
	 * @return 成功返回true
	 */
	public boolean sendGetRequest(){
		// XXX 2014-08-11
		mHttpGet = new HttpGet(mHttpUrl);        //生产请求对象
		mHttpClient =new DefaultHttpClient();    //生产HTTP客户端请求对象
		try {
			HttpRequest mHttpRequest = new HttpRequest(0, mHttpGet, mHttpClient);
			Thread httpThread = new Thread(mHttpRequest);
			httpThread.start();
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 发送POST请求方法
	 * 
	 * @param  valuePairs   请求数据
	 * 
	 * @return 成功返回true
	 */
	public boolean sendPostRequest(ArrayList<NameValuePair> valuePairs){
		// XXX 2014-08-11
		mHttpPost = new HttpPost(mHttpUrl);      //生产请求对象
		mHttpClient =new DefaultHttpClient();    //生产HTTP客户端请求对象
		try {
			HttpRequest mHttpRequest = new HttpRequest(1, mHttpPost, mHttpClient, valuePairs);
			Thread httpThread = new Thread(mHttpRequest);
			httpThread.start();
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * HTTP请求线程
	 */
	public class HttpRequest implements Runnable{
		/** 私有成员变量*/
		private int mHttpType;                 //请求类型 0：GET 1：POST
		private HttpResponse mHttpResponse;    //HTTP响应
		private HttpEntity   mHttpEntity;      //HTTP响应实体
		private InputStream  mInputStream;     //数据流
		private HttpGet mHttpGet;              //GET请求对象
		private HttpPost mHttpPost;            //POST请求对象
		private HttpClient mHttpClient;        //http客户端请求对象
		private List<NameValuePair> postValue; //POST方式传值
		
		public HttpRequest(int HttpType, HttpGet mHttpGet, HttpClient mHttpClient) {
			// TODO Auto-generated constructor stub
			this.mHttpType     = HttpType;
			this.mHttpGet      = mHttpGet;
			this.mHttpClient   = mHttpClient;
			this.mHttpResponse = null;
			this.mHttpEntity   = null;
			this.mInputStream  = null;
		}
		public HttpRequest(int HttpType, HttpPost mHttpPost, HttpClient mHttpClient, ArrayList<NameValuePair> value) {
			// TODO Auto-generated constructor stub
			this.mHttpType     = HttpType;
			this.mHttpPost     = mHttpPost;
			this.mHttpClient   = mHttpClient;
			this.mHttpResponse = null;
			this.mHttpEntity   = null;
			this.mInputStream  = null;
			this.postValue     = value;
		}
		
		@Override
		public void run(){
			switch (mHttpType) {
			case 0:
				httpGetDeal();
				break;
				
			case 1:
				httpPostDeal();
				break;

			default:
				break;
			}
		}
		
		/** http post 处理*/
		public void httpPostDeal(){
			try {
				mHttpEntity = new UrlEncodedFormEntity(postValue, HTTP.UTF_8); //实体(统一编码UTF-8)
				mHttpPost.setEntity(mHttpEntity);                              //将请求内容加入到请求中
				try {
					mHttpResponse = mHttpClient.execute(mHttpPost);            //发送请求并获得响应对象
					if (mHttpResponse != null) {
						mHttpEntity   = mHttpResponse.getEntity();             //获得响应实体
						if (mHttpEntity != null) {
							mInputStream  = mHttpEntity.getContent();              //或的输入流
							BufferedReader mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream)); //从输入流获得流文件对象
							String line = "";
							while((line = mBufferedReader.readLine()) != null){
								mResult += line;
							}
						}
					}
					mDoneFlag = true;
					//返回结果后，发送消息通知UI线程
					if (mHandler!=null) {
						mHandler.sendEmptyMessage(IS_DONE);
					}
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/** http get 处理*/
		public void httpGetDeal(){
			try {
				mHttpResponse = mHttpClient.execute(mHttpGet);     //发送请求并获得响应对象
				if (mHttpResponse != null) {
					mHttpEntity   = mHttpResponse.getEntity();     //获得响应实体
					if (mHttpEntity != null) {
						mInputStream  = mHttpEntity.getContent();      //或的输入流
						BufferedReader mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream)); //从输入流获得流文件对象
						String line = "";
						while((line = mBufferedReader.readLine()) != null){
							mResult += line;
						}
					}
				}
				mDoneFlag = true;
				if (mHandler!=null) {
					mHandler.sendEmptyMessage(IS_DONE);
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally{
				try {
					mInputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
