package com.wdl.view;

import com.wdl.asynctask.AsyncTask4SendSuggestion;
import com.wdl.myclass.Constant;
import com.wdl.myclass.SqliteUtil;
import com.wdl.wandianle.R;

import android.app.ActionBar;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SugActivity extends Activity {
	private Button btnBack;
	private Button btnSend;
	private EditText etText;
	private ActionBar mActionBar;
	private SqliteUtil mSqliteUtil;
	private Cursor mCursor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sug);
		init();
	}
	
	private void init(){
		mActionBar = this.getActionBar();
		mActionBar.hide();
		
		btnBack = (Button) this.findViewById(R.id.back_sug);
		btnSend = (Button) this.findViewById(R.id.sug_send);
		etText = (EditText) this.findViewById(R.id.sug_text);
		btnBack.setOnClickListener(new BtnOnClickListener());
		btnSend.setOnClickListener(new BtnOnClickListener());
		etText.requestFocus();
		mSqliteUtil = new SqliteUtil(SugActivity.this, null);
		mCursor = mSqliteUtil.query(Constant.TABLE_USER, "*", Constant.ID + "=" + Constant.TABLE_ID_KEY, "", -1, -1);
	}

private class BtnOnClickListener implements OnClickListener{
		
		@Override
		public void onClick(View v){
			switch (Integer.parseInt(v.getTag().toString())) {
			case 0:
				finish();
				break;
				
			case 1://发送
				if (etText.getText().toString().equals("")) {
					Toast.makeText(SugActivity.this, getResources().getString(R.string.msg_input_not_null), Toast.LENGTH_SHORT).show();
					return;
				}
				//TODO 发送反馈
				mCursor.moveToFirst();
				if (mCursor.getCount() == 0) {
					Toast.makeText(SugActivity.this, getResources().getString(R.string.msg_db_read_error), Toast.LENGTH_SHORT).show();
				}else {
					AsyncTask4SendSuggestion mAsyncTask4SendSuggestion = new AsyncTask4SendSuggestion(SugActivity.this);
					mAsyncTask4SendSuggestion.execute(new String[]{mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NAME)), mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_PASSWORD)), etText.getText().toString()});
				}
				etText.setText("");
				break;

			default:
				break;
			}
		}
	}
}
