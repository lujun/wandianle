package com.wdl.view;

import com.wdl.myclass.Constant;
import com.wdl.myclass.SystemSet;
import com.wdl.wandianle.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class SearchActivity extends Activity implements OnClickListener {
	private EditText etKeyWords;
	private ImageButton mSearch;
	private Intent mIntent;
	private Button btnBack;
	private ActionBar mActionBar;
	private RelativeLayout rlSearch;
	
	private Animation mAnimation;
	private SystemSet mSystemSet;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		init();
	}

	private void init(){
		mActionBar = this.getActionBar();
		mActionBar.hide();
		etKeyWords =(EditText) this.findViewById(R.id.keywords);
		btnBack = (Button) this.findViewById(R.id.back_search);
		mSearch = (ImageButton) this.findViewById(R.id.main_search);
		rlSearch = (RelativeLayout) this.findViewById(R.id.rl_search);
		mSearch.setOnClickListener(this);
		btnBack.setOnClickListener(this);
		mAnimation = AnimationUtils.loadAnimation(SearchActivity.this, R.anim.common_anim_r2l);
		mAnimation.setAnimationListener(new OnAnimationListener());
		rlSearch.setAnimation(mAnimation);
	}
	
	@Override
	public void onClick(View v){
		switch (Integer.parseInt(v.getTag().toString())) {
		case 0:
			break;
			
		case 1:
			if (etKeyWords.getText().toString().equals("")) {
				Toast.makeText(SearchActivity.this, getResources().getString(R.string.msg_no_keywords), Toast.LENGTH_SHORT).show();
				return;
			}
			mIntent = new Intent();
			mIntent.putExtra(Constant.TRAINNO_KEY, etKeyWords.getText().toString());
			setResult(5, mIntent);
			break;

		default:
			break;
		}
		finish();
	}
	
	private class OnAnimationListener implements AnimationListener{

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			// TODO Auto-generated method stub
			etKeyWords.requestFocus();
			mSystemSet = new SystemSet(SearchActivity.this);
			mSystemSet.showOrHideInputMethodManager();
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
		}
	}
}
