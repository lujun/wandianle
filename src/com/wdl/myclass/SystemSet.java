package com.wdl.myclass;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

/**
 * 系统设置类
 * 
 * @author lujun
 * @since  2014-08-19
 *
 */
public class SystemSet {
	private Activity mActivity;
	
	public SystemSet(Activity activity){
		this.mActivity = activity;
	}
	
	/**
	 * 获取当前屏幕亮度值
	 * 
	 * @return (int)当前屏幕的亮度值
	 */
	public int getScreenBrightness(){
		int screenBrightness = 255;
		try {
			screenBrightness = Settings.System.getInt(mActivity.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return screenBrightness; 
	}
	
	/**
	 * 设置屏幕亮度
	 * 
	 * @param screenBrightness 需要设置的屏幕亮度值（0-255）
	 */
	public void setScreenBrightness(int screenBrightness){
		try {
			Settings.System.putInt(mActivity.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, screenBrightness);
			Window window = mActivity.getWindow();
			WindowManager.LayoutParams locaLayoutParams = window.getAttributes();
			float f = screenBrightness / 255.0F;
			locaLayoutParams.screenBrightness = f;
			window.setAttributes(locaLayoutParams);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	/**
	 * 设置当前屏幕亮度的模式 
	 * SCREEN_BRIGHTNESS_MODE_AUTOMATIC=1 为自动调节屏幕亮度
	 * SCREEN_BRIGHTNESS_MODE_MANUAL=0 为手动调节屏幕亮度
	 * 
	 * @param paramInt (int) 需要设置的亮度模式
	 */
	private void setScreenMode(int paramInt) {
		try {
			Settings.System.putInt(mActivity.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, paramInt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 获得当前屏幕亮度的模式 
	 * SCREEN_BRIGHTNESS_MODE_AUTOMATIC=1 为自动调节屏幕亮度
	 * SCREEN_BRIGHTNESS_MODE_MANUAL=0 为手动调节屏幕亮度
	 * 
	 * @return (int) 亮度模式
	 */
	private int getScreenMode() {
		int screenMode = 0;
		try {
			screenMode = Settings.System.getInt(mActivity.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return screenMode;
	}
	
	/**
	 * 自动显示或隐藏软键盘静态方法
	 */
	public void showOrHideInputMethodManager(){
		InputMethodManager mInputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
		mInputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
	}
	
	/**
	 * 强制隐藏软键盘方法
	 * 
	 * @param activity 当前软键盘所在的activity
	 */
	public void hideInputMethodManager(Activity activity){
		InputMethodManager mInputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
		mInputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
	}
}
