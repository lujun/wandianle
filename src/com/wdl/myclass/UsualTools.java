package com.wdl.myclass;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import com.wdl.wandianle.R;

import android.content.Context;
import android.content.Intent;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.hardware.Camera;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

/**
 * 常用工具类
 * 
 * @author lujun
 * @since  2014-08-28
 */
public class UsualTools {

	/**
	 * 将Bitmap图片保存方法
	 * 
	 * @param where         0,手机存储；1-SD卡
	 * @param rlativePath   保存路径，如/test/pic/
	 * @param bm            Bitmap
	 * @return              成功返回true
	 */
	public static boolean saveBitmap(Context context, int where, String pathName, Bitmap bm, String picName){
		String outPath = "";
		if (where == 1) {
			if (!FileOperation.isSDCardHangUp()) {
				return false;
			}
			outPath = Environment.getExternalStorageDirectory() + pathName;
		}else if (where == 0) {
			outPath = FileOperation.getPackagePath(context, "").get(0) + pathName;
		}
		try {
			File path = new File(outPath);
			File file = new File(outPath + picName);
			if (!path.exists()) {
				path.mkdirs();
			}
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream mFileOutputStream = new FileOutputStream(file);
			if (!bm.compress(Bitmap.CompressFormat.PNG, 100, mFileOutputStream)) {
				return false;
			}
			mFileOutputStream.flush();
			mFileOutputStream.close();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * 读取图片并转换成Bitmap类型
	 * 
	 * @param  path 绝对路径
	 * 
	 * @return 成功返回Bitmap，读取失败返回null
	 */
	public static Bitmap readPic(String path){
		File file = new File(path);
		if (!file.exists()) {
			return null;
		}
		try {
			Bitmap bm = BitmapFactory.decodeFile(path);
			if (bm != null) {
				return bm;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	
	/**   
	  * 分享功能   
	  * @param context 上下文   
	  * @param activityTitle Activity的名字   
	  * @param msgTitle 消息标题   
	  * @param msgText 消息内容   
	  * @param imgPath 图片路径，不分享图片则传null   
	  */    
	public static void shareMsg(Context context, String activityTitle,String msgTitle, String msgText, String imgPath) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		if (imgPath == null || imgPath.equals("")) {
			intent.setType("text/plain"); // 纯文本
		} else {
			File f = new File(imgPath);
			if (f != null && f.exists() && f.isFile()) {
				intent.setType("image/png");
				Uri u = Uri.fromFile(f);
				intent.putExtra(Intent.EXTRA_STREAM, u);
			}
		}
		intent.putExtra(Intent.EXTRA_SUBJECT, msgTitle);
		intent.putExtra(Intent.EXTRA_TEXT, msgText);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(Intent.createChooser(intent, activityTitle));
	}
	
	/**
	 * 开启关闭闪光灯方法
	 * 
	 * @param context 上下文环境
	 * @param flag 0 关闭闪光灯
	 *             1 开启闪光灯
	 */
	public static boolean camearFlash(Context context,int flag, Camera camera){
		PackageManager mPackageManager = context.getPackageManager();
		FeatureInfo[] mFeatureInfos = mPackageManager.getSystemAvailableFeatures();
		for (FeatureInfo featureInfo : mFeatureInfos) {
			if (PackageManager.FEATURE_CAMERA_FLASH.equals(featureInfo.name)) {
				if (flag == 0) {
					if (camera != null) {
						camera.stopPreview();
						camera.release();
						camera=null;
					}
				}else if (flag == 1) {
					if (camera == null) {
						camera = Camera.open();
					}
					Camera.Parameters mParameters = camera.getParameters();
					mParameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
					camera.setParameters(mParameters);
					camera.startPreview();
				}
			}
		}
		
		return false;
	}
	
	/**
	 * 下载图片
	 * 
	 * @param bmurl
	 * @return Bitmap
	 */
	public static Bitmap downloadBitmap(String bmurl) {
		Bitmap bm = null;
		InputStream is = null;
		BufferedInputStream bis = null;
		try {
			URL url = new URL(bmurl);
			URLConnection connection = url.openConnection();
			bis = new BufferedInputStream(connection.getInputStream());
			bm = BitmapFactory.decodeStream(bis);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (bis != null)
					bis.close();
				if (is != null)
					is.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return bm;
	}
	
	/**
	 * 使用系统浏览器打开网页方法
	 * 
	 * @param context 上下文
	 * @param url     需要打开的网页地址
	 */
	public static void goBowser(Context context, String url){
		//检查是否有浏览器
		PackageManager mPackageManager = context.getPackageManager();
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.addCategory(intent.CATEGORY_BROWSABLE);
		intent.setData(Uri.parse("http://"));
		List<ResolveInfo> mList = mPackageManager.queryIntentActivities(intent, PackageManager.GET_INTENT_FILTERS);
		if (!(((mList == null) ? 0 : mList.size()) > 0)) {
			Toast.makeText(context, context.getResources().getString(R.string.msg_no_bowser), Toast.LENGTH_SHORT).show();
			return;
		}
		//有则跳转
		intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(url));
		context.startActivity(intent);
	}
	
	/**
	 * 获取圆角图片方法
	 * 
	 * @param bitmap  需要设置的位图
	 * @param pixels  需要圆角的角度，数值越大越圆
	 * @return        返回BitMap位图
	 */
	public static Bitmap toRoundCorner(Bitmap bitmap, int pixels) {  
	    Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),  
	            bitmap.getHeight(), Config.ARGB_8888);  
	    Canvas canvas = new Canvas(output);  
	    final int color = 0xff424242;  
	    final Paint paint = new Paint();  
	    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());  
	    final RectF rectF = new RectF(rect);  
	    final float roundPx = pixels;  
	    paint.setAntiAlias(true);  
	    canvas.drawARGB(0, 0, 0, 0);  
	    paint.setColor(color);  
	    canvas.drawRoundRect(rectF, roundPx, roundPx, paint);  
	    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));  
	    canvas.drawBitmap(bitmap, rect, rect, paint);  
	    return output;  
	}  
	
	
	/**
	 * 判断手机是否支持GPS
	 * 
	 * @param context
	 * @return
	 */
	public static boolean hasGPSDevice(Context context) {
		final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		if (locationManager == null)
			return false;
		final List<String> providers = locationManager.getAllProviders();
		if (providers == null)
			return false;
		return providers.contains(LocationManager.GPS_PROVIDER);
	}
}
