package com.wdl.wandianle;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.image.SmartImageView;
import com.wdl.asynctask.AsyncTask4UDShareCon;
import com.wdl.asynctask.AsyncTask4UpdateUserInfo;
import com.wdl.myclass.Constant;
import com.wdl.myclass.FileOperation;
import com.wdl.myclass.HttpOperation;
import com.wdl.myclass.NetWorkInfo;
import com.wdl.myclass.ParseData;
import com.wdl.myclass.SqliteUtil;
import com.wdl.pull2freshlistview.PullToRefreshBase;
import com.wdl.pull2freshlistview.PullToRefreshBase.OnRefreshListener;
import com.wdl.pull2freshlistview.PullToRefreshListView;
import com.wdl.view.DoorActivity;
import com.wdl.view.SearchActivity;
import com.wdl.view.SetActivity;
import com.wdl.view.ShareActivity;
import com.wdl.view.ShareDetailActivity;
import com.wdl.view.SugActivity;
import com.wdl.view.UserCardActivity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	/** Widget */
	private ActionBar mActionBar;
	private ListView mListView;
	private BaseAdapter mBaseAdapter;
	/** class*/
	private SqliteUtil mSqliteUtil;
	private PullToRefreshListView mPullToRefreshListView;
	private HttpOperation mHttpOperation;
	private ParseData mParseData;
	private AsyncTask4UDShareCon mAsyncTask4UDShareCon;
	/** Android Native */
	private JSONObject mJsonObject;
	private Intent mIntent;
	private Cursor mCursor;
	private ArrayList<Map<String, Object>> mArrayList;
	private boolean mIsStart;
	private boolean mThreadFlag;
	private boolean mFirstInFlag;
	private String trainno;
	private String lastupdate;
	private SimpleDateFormat mDateFormat;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPullToRefreshListView = new PullToRefreshListView(this);
		setContentView(mPullToRefreshListView);
		init();
	}

	private void init() {
		initActionBar();
		mIsStart = true;
		mThreadFlag = true;//刷新与加载更多标志位TRUE为生产者生产，false为消费者消费
		mFirstInFlag = true;
		trainno = "";
		lastupdate = ""; 
		mDateFormat = new SimpleDateFormat("MM-dd HH:mm");
		mPullToRefreshListView.setPullLoadEnabled(false);
		mPullToRefreshListView.setScrollLoadEnabled(true);
		mPullToRefreshListView.doPullRefreshing(true, 500);
		mPullToRefreshListView.setOnRefreshListener(new ListViewOnRefreshListener());
		mListView = mPullToRefreshListView.getRefreshableView();

		mArrayList = new ArrayList<Map<String, Object>>();
		mBaseAdapter = new ShareConAdapter(this);
		mListView.setAdapter(mBaseAdapter);
		mListView.setOnItemClickListener(new ListViewItemOnClick());

		setLastUpdateTime();
		mParseData = new ParseData();
		mSqliteUtil = new SqliteUtil(MainActivity.this, null);
		mCursor = mSqliteUtil.query(Constant.TABLE_USER, "*", Constant.ID + "=" + Constant.TABLE_ID_KEY, "", -1, -1);
		if (mCursor.getCount() > 0) {
			mCursor.moveToFirst();
			if (NetWorkInfo.isNetWorkConnected(MainActivity.this)) {
				AsyncTask4UpdateUserInfo mAsyncTask4UpdateUserInfo = new AsyncTask4UpdateUserInfo(MainActivity.this);
				mAsyncTask4UpdateUserInfo.execute(Constant.SERVER_USER_AVATAR_URL + mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NAME)) + ".png");
			} else {
				Toast.makeText(MainActivity.this, getResources().getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
			}
		}
		mCursor.close();
	}

	private void initActionBar() {
		mActionBar = this.getActionBar();
		mActionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_title));
		mActionBar.setDisplayShowTitleEnabled(false);
		mActionBar.setDisplayUseLogoEnabled(false);
		mActionBar.setDisplayHomeAsUpEnabled(false);
		mActionBar.setIcon(getResources().getDrawable(R.drawable.logo));
		getOverflowMenu();
	}

	// force to show overflow menu in actionbar for android 4.4 below
	private void getOverflowMenu() {
		try {
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
			if (menuKeyField != null) {
				menuKeyField.setAccessible(true);
				menuKeyField.setBoolean(config, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.layout.actionbar_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_share:// 分享
			mIntent = new Intent(MainActivity.this, ShareActivity.class);
			break;

		case R.id.menu_search:// 检索
			mIntent = new Intent(MainActivity.this, SearchActivity.class);
			break;

		case R.id.menu_set:// 设置
			mIntent = new Intent(MainActivity.this, SetActivity.class);
			break;

		case R.id.menu_me:// 个人中心
			mIntent = new Intent(MainActivity.this, UserCardActivity.class);
			break;

		case R.id.menu_sug:// 意见反馈
			mIntent = new Intent(MainActivity.this, SugActivity.class);
			break;

		default:
			break;
		}
		startActivityForResult(mIntent, 0);
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onMenuOpened(int featureId, Menu menu) {// 反射机制显示menu项的ICON
		if (featureId == Window.FEATURE_ACTION_BAR && menu != null) {
			if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
				try {
					Method method = menu.getClass().getDeclaredMethod("setOptionalIconsVisible", Boolean.TYPE);
					method.setAccessible(true);// 类内部成员变量是private的，需要调用此方法
					method.invoke(menu, true);// 实现方法体的功能
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return super.onMenuOpened(featureId, menu);
	}

	private class BtnOnClickListener implements OnClickListener {
		private String mPosition;

		public BtnOnClickListener(String arg0) {
			this.mPosition = arg0;
		}

		@Override
		public void onClick(View v) {
			mCursor = mSqliteUtil.query(Constant.TABLE_USER, "*", Constant.ID + "=" + Constant.TABLE_ID_KEY, "", -1, -1);
			if (mCursor.getCount() == 0) {
				Toast.makeText(MainActivity.this, getResources().getString(R.string.msg_operate_failed), Toast.LENGTH_SHORT).show();
				mCursor.close();
				return;
			}
			mCursor.moveToFirst();
			switch (Integer.parseInt(v.getTag().toString())) {
			case 1:// 顶
				mAsyncTask4UDShareCon = new AsyncTask4UDShareCon(MainActivity.this, 1);
				mAsyncTask4UDShareCon.execute(new String[]{mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NAME)), mArrayList.get(Integer.parseInt(mPosition)).get("id").toString()});
				break;

			case 2:// 踩
				mAsyncTask4UDShareCon = new AsyncTask4UDShareCon(MainActivity.this, 2);
				mAsyncTask4UDShareCon.execute(new String[]{mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NAME)), mArrayList.get(Integer.parseInt(mPosition)).get("id").toString()});
				break;

			default:
				break;
			}
		}
	}

	private class ShareConAdapter extends BaseAdapter {
		private LayoutInflater mLayoutInflater;

		public ShareConAdapter(Context context) {
			this.mLayoutInflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			return mArrayList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ShareConViewHolder mShareConViewHolder = null;
			if (convertView == null) {
				mShareConViewHolder = new ShareConViewHolder();
				convertView = this.mLayoutInflater.inflate(R.layout.linearlayout_one, null);
				mShareConViewHolder.mImageView = (SmartImageView) convertView.findViewById(R.id.user_avatar);
				mShareConViewHolder.mConText = (TextView) convertView.findViewById(R.id.current_text);
				mShareConViewHolder.mTrainNo = (TextView) convertView.findViewById(R.id.train_no);
				mShareConViewHolder.mTrainLocation = (TextView) convertView.findViewById(R.id.train_location);
				mShareConViewHolder.mLateTime = (TextView) convertView.findViewById(R.id.train_time);
				mShareConViewHolder.mAgree = (ImageButton) convertView.findViewById(R.id.u_like);
				mShareConViewHolder.mDisagree = (ImageButton) convertView.findViewById(R.id.u_hate);
				convertView.setTag(mShareConViewHolder);
			} else {
				mShareConViewHolder = (ShareConViewHolder) convertView.getTag();
			}
			// TODO 设置数据
			mShareConViewHolder.mImageView.setImageUrl(Constant.SERVER_USER_AVATAR_URL + mArrayList.get(position).get("username").toString() + ".png");
			mShareConViewHolder.mConText.setText(mArrayList.get(position).get("context").toString());
			mShareConViewHolder.mTrainNo.setText(mArrayList.get(position).get("trainno").toString().toUpperCase());
			mShareConViewHolder.mTrainLocation.setText(mArrayList.get(position).get("trainlocation").toString());
			mShareConViewHolder.mLateTime.setText(mArrayList.get(position).get("latetime").toString());
			mShareConViewHolder.mDisagree.setOnClickListener(new BtnOnClickListener(Integer.toString(position)));
			mShareConViewHolder.mAgree.setOnClickListener(new BtnOnClickListener(Integer.toString(position)));
			return convertView;
		}
	}

	private class ShareConViewHolder {
		public SmartImageView mImageView;
		public TextView mConText;
		public TextView mTrainNo;
		public TextView mTrainLocation;
		public TextView mLateTime;
		public ImageButton mAgree;
		public ImageButton mDisagree;
	}

	private class ListViewItemOnClick implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			Bundle bundle = new Bundle();
			bundle.putString("id", mArrayList.get(arg2).get("id").toString());
			bundle.putString("username", mArrayList.get(arg2).get("username").toString());
			bundle.putString("trainno", mArrayList.get(arg2).get("trainno").toString());
			bundle.putString("context", mArrayList.get(arg2).get("context").toString());
			bundle.putString("time", mArrayList.get(arg2).get("time").toString());
			bundle.putString("longitude", mArrayList.get(arg2).get("longitude").toString());
			bundle.putString("latitude", mArrayList.get(arg2).get("latitude").toString());
			bundle.putString("trainlocation", mArrayList.get(arg2).get("trainlocation").toString());
			bundle.putString("latetime", mArrayList.get(arg2).get("latetime").toString());
			bundle.putString("up", mArrayList.get(arg2).get("up").toString());
			bundle.putString("down", mArrayList.get(arg2).get("down").toString());
			mIntent = new Intent(MainActivity.this, ShareDetailActivity.class);
			mIntent.putExtras(bundle);
			startActivity(mIntent);
		}
	}

	private class ListViewOnRefreshListener implements OnRefreshListener<ListView> {
		@Override
		public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
			if (!NetWorkInfo.isNetWorkConnected(MainActivity.this)) {
				Toast.makeText(MainActivity.this, getResources().getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
				mPullToRefreshListView.onPullDownRefreshComplete();
				mPullToRefreshListView.onPullUpRefreshComplete();
				return;
			}
			if (mThreadFlag) {
				mThreadFlag = false;
				mIsStart = true;
				new AsyncTask4UpdateCon().execute("");
			}
		}

		@Override
		public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
			if (!NetWorkInfo.isNetWorkConnected(MainActivity.this)) {
				Toast.makeText(MainActivity.this, getResources().getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
				mPullToRefreshListView.onPullDownRefreshComplete();
				mPullToRefreshListView.onPullUpRefreshComplete();
				return;
			}
			if (mThreadFlag) {
				mThreadFlag = false;
				mIsStart = false;
				new AsyncTask4UpdateCon().execute("");
			}
		}
	}

	private void setLastUpdateTime() {
		String text = formatDateTime(System.currentTimeMillis());
		mPullToRefreshListView.setLastUpdatedLabel(text);
	}

	private String formatDateTime(long time) {
		if (0 == time) {
			return "";
		}
		return mDateFormat.format(new Date(time));
	}

	private class AsyncTask4UpdateCon extends AsyncTask<String, Integer, Boolean> {

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				if (mIsStart) {//上拉刷新
					if (mArrayList != null && mArrayList.size() > 0 && mArrayList.get(0) != null && mArrayList.get(0).get("time") != null) {
						lastupdate = (String) mArrayList.get(0).get("time");
						mFirstInFlag = false;
					}
					mHttpOperation = new HttpOperation(0, Constant.SERVER_CON_URL, "start=0" + "&end=" + Constant.ONE_PAGE_NUM + "&lastupdate=" + lastupdate + "&trainno=" + trainno, null);
					mHttpOperation.setHttpRequest(null);
					trainno = "";
					while(!mHttpOperation.mDoneFlag){}
					mJsonObject = new JSONObject(mHttpOperation.mResult);
					if (mJsonObject.getString(Constant.JSON_RESULT_HEAD) == null || mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("") || mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("2220")) {
						return false;
					}else if(mJsonObject.getInt(Constant.JSON_TOTAL_HEAD) == 0){
						return false;
					}else if(mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("2221")) {
						ArrayList<ArrayList<String>> tmpArrayLists = mParseData.parseJsonData(Constant.JSON_CON_HEAD, "{\"" + Constant.JSON_CON_HEAD + "\":" + mJsonObject.getString(Constant.JSON_CON_HEAD) + "}", Constant.JSON_CON_KEYS);
						if (tmpArrayLists.size() == 0) {
							return false;
						}else {
//							mArrayList.clear();
							for (ArrayList<String> arrayList : tmpArrayLists) {
								Map<String, Object> map = new HashMap<String, Object>();
								map.put("id", arrayList.get(0));
								map.put("username", arrayList.get(1));
								map.put("trainno", arrayList.get(2));
								map.put("context", arrayList.get(3));
								map.put("time", arrayList.get(4));
								map.put("longitude", arrayList.get(5));
								map.put("latitude", arrayList.get(6));
								map.put("trainlocation", arrayList.get(7));
								map.put("latetime", arrayList.get(8));
								map.put("up", arrayList.get(9));
								map.put("down", arrayList.get(10));
								if (mFirstInFlag) {
									mArrayList.add(map);
								}else {
									mArrayList.add(0, map);
								}
							}
							return true;
						}
					}else {
						return false;
					}
				}else {//下拉加载更多
					mHttpOperation = new HttpOperation(0, Constant.SERVER_CON_URL, "start="+ (mListView.getCount() - 1) + "&end=" + Constant.ONE_PAGE_NUM + "&lastupdate=" + "&trainno=" + trainno, null);
					mHttpOperation.setHttpRequest(null);
					while(!mHttpOperation.mDoneFlag){}
					mJsonObject = new JSONObject(mHttpOperation.mResult);
					if (mJsonObject.getString(Constant.JSON_RESULT_HEAD) == null || mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("") || mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("2220")) {
						return false;
					}else if(mJsonObject.getInt(Constant.JSON_TOTAL_HEAD) == 0){
						return false;
					}else if(mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("2221")) {
						ArrayList<ArrayList<String>> tmpArrayLists = mParseData.parseJsonData(Constant.JSON_CON_HEAD, "{\"" + Constant.JSON_CON_HEAD + "\":" + mJsonObject.getString(Constant.JSON_CON_HEAD) + "}", Constant.JSON_CON_KEYS);
						if (tmpArrayLists.size() == 0) {
							return false;
						}else {
							for (ArrayList<String> arrayList : tmpArrayLists) {
								Map<String, Object> map = new HashMap<String, Object>();
								map.put("id", arrayList.get(0));
								map.put("username", arrayList.get(1));
								map.put("trainno", arrayList.get(2));
								map.put("context", arrayList.get(3));
								map.put("time", arrayList.get(4));
								map.put("longitude", arrayList.get(5));
								map.put("latitude", arrayList.get(6));
								map.put("trainlocation", arrayList.get(7));
								map.put("latetime", arrayList.get(8));
								map.put("up", arrayList.get(9));
								map.put("down", arrayList.get(10));
								mArrayList.add(map);
							}
							return true;
						}
					}else {
						return false;
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			if (result) {
				setLastUpdateTime();
				mBaseAdapter.notifyDataSetChanged();
			}else {
				try {
					if (mJsonObject.getInt(Constant.JSON_TOTAL_HEAD) == 0) {
						Toast.makeText(MainActivity.this, getResources().getString(R.string.msg_nomore_data), Toast.LENGTH_SHORT).show();
//						mPullToRefreshListView.setHasMoreData(false);
					}else {
						Toast.makeText(MainActivity.this, getResources().getString(R.string.msg_parse_error), Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			mPullToRefreshListView.onPullDownRefreshComplete();
			mPullToRefreshListView.onPullUpRefreshComplete();
			setLastUpdateTime();
			mThreadFlag = true;
			super.onPostExecute(result);
		}
	}

	@Override
	public void onActivityResult(int RequestCode, int resultCode, Intent data) {
		super.onActivityResult(resultCode, resultCode, data);
		if (RequestCode == 0 && resultCode == 2) {
			ArrayList<Object> tmpArrayList = new ArrayList<Object>();
			tmpArrayList.add(Constant.TABLE_USER_NAME);
			tmpArrayList.add("");
			tmpArrayList.add(Constant.TABLE_USER_PASSWORD);
			tmpArrayList.add("");
			tmpArrayList.add(Constant.TABLE_USER_NICKNAME);
			tmpArrayList.add("");
			tmpArrayList.add(Constant.TABLE_USER_SINCON);
			tmpArrayList.add("");
			tmpArrayList.add(Constant.TABLE_USER_SEX);
			tmpArrayList.add("");
			tmpArrayList.add(Constant.TABLE_USER_HEADURL);
			tmpArrayList.add("");
			tmpArrayList.add(Constant.TABLE_USER_PHONE);
			tmpArrayList.add("");
			tmpArrayList.add(Constant.TABLE_USER_SCORE);
			tmpArrayList.add("");
			tmpArrayList.add(Constant.TABLE_USER_CANSHARE);
			tmpArrayList.add("");
			if (!mSqliteUtil.update(Constant.TABLE_USER, tmpArrayList, Constant.ID + "=" + Constant.TABLE_ID_KEY)) {
				Toast.makeText(MainActivity.this, getResources().getString(R.string.msg_logout_failed), Toast.LENGTH_SHORT).show();
				return;
			}
			File file = new File(FileOperation.getPackagePath(MainActivity.this, "").get(0) + Constant.USER_IMG_PATH + Constant.USER_AVATAR_NAME);
			if (file.exists()) {
				file.delete();
			}
			mIntent = new Intent(MainActivity.this, DoorActivity.class);
			startActivity(mIntent);
			finish();
		}else if (RequestCode == 0 && resultCode == 5) {
			trainno = data.getStringExtra(Constant.TRAINNO_KEY);
			if (mArrayList != null && mArrayList.size() != 0) {
				mArrayList.clear();
			}
			mPullToRefreshListView.doPullRefreshing(true, 500);
		}
	}
}
