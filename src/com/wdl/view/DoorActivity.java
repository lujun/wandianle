package com.wdl.view;

import com.wdl.wandianle.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class DoorActivity extends Activity {
	private ActionBar mActionBar;
	private Button btnReg;
	private Button btnLogin;
	
	private Intent mIntent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_door);
		init();
	}

	private void init() {
		mActionBar = this.getActionBar();
		mActionBar.hide();
		
		btnReg = (Button) this.findViewById(R.id.btn_door_reg);
		btnLogin = (Button) this.findViewById(R.id.btn_door_login);
		
		btnReg.setOnClickListener(new BtnOnClickListener());
		btnLogin.setOnClickListener(new BtnOnClickListener());
	}
	
	private class BtnOnClickListener implements OnClickListener{
		
		@Override
		public void onClick(View v){
			switch (Integer.parseInt(v.getTag().toString())) {
			case 0:
				mIntent = new Intent(DoorActivity.this, RegActivity.class);
				break;
				
			case 1:
				mIntent = new Intent(DoorActivity.this, LoginActivity.class);
				break;

			default:
				break;
			}
			startActivity(mIntent);
			finish();
		}
	}

}
