package com.wdl.myclass;

import java.util.ArrayList;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Handler;

/**
 * 数据库辅助操作类
 * 
 * @author lujun
 * @since  2014-08-05
 *
 */
public class SqliteUtil {
	/** 数据库辅助类*/
	private SqliteUtilHelper mSqliteUtilHelper;
	private SQLiteDatabase   mSqliteDatabase;
	private Handler mHandler;
	private static final int IS_DONE = 2;
	
	/**
	 * 构造函数
	 * @param context 上下文环境
	 * @param handler 消息处理示例，消息处理完成后返回消息what值为2。不需要消息处理为null
	 */
	public SqliteUtil(Context context,Handler handler) {
		// TODO Auto-generated constructor stub
		this.mSqliteUtilHelper = new SqliteUtilHelper(context, Constant.DB_NAME, null, Constant.DB_VERSION);
		this.mSqliteDatabase   = this.mSqliteUtilHelper.getWritableDatabase();
		this.mHandler          = handler;
	}
	
	/**
	 * 增加数据方法
	 * 
	 * @param table 表名称
	 * @param value 数据,模式为{"key",value,"key",value}，添加必须对应。 如：{"user_cid",14113901103,"user_age",21}
	 * 
	 * @return 增加成功返回true
	 */
	public boolean insert(String table, ArrayList<Object> value){
		// XXX 2014-08-07
		if (value.size()%2 != 0) {
			return false;
		}
		mSqliteDatabase.beginTransaction();               //开始事务，保证数据的完整性
		try {
			String keys     = "";
			String bindChar = "";
			ArrayList<Object> values = new ArrayList<Object>();
			
			//另一种方式
			/*ContentValues contentValues = new ContentValues();
			for(Iterator iterator = value.iterator();iterator.hasNext();){
				contentValues.put(iterator.next(), iterator.next());
			}
			long rowId = sqLiteDatabase.insert(table, null, contentValues);
			*/

			for (int i = 0; i < (value.size()/2); i++) {
				keys     += value.get(i*2) + ",";
				bindChar += "?,";
				values.add(value.get(i*2+1));
			}
			keys     = keys.substring(0, keys.length() - 1);
			bindChar = bindChar.substring(0, bindChar.length() - 1);
			Object[] bindArgs = (Object[]) values.toArray(new Object[value.size()/2]);
			try {
				mSqliteDatabase.execSQL("INSERT INTO " + table + "(" +keys+ ")" + "VALUES("+ bindChar +")", bindArgs);
			} catch (SQLiteException e) {
				// TODO: handle exception
				e.printStackTrace();
				return false;
			}
			mSqliteDatabase.setTransactionSuccessful();   //设置事务成功完成
			return true;
		}finally{
			mSqliteDatabase.endTransaction();             //结束事务
		}
	}
	
	/**
	 * 修改数据方法
	 * 
	 * @param table       表名
	 * @param value       更新值,模式为{"key",value,"key",value}，添加必须对应。如：{"user_cid",14113901103,"user_age",21}
	 * @param whereClause 条件，如：user_name="lujun",没有条件为null或""
	 * 
	 * @rerurn 修改成功，返回true
	 */
	public boolean update(String table, ArrayList<Object> value, String whereClause){
		// XXX 2014-08-07
		if (value.size()%2 != 0) {
			return false;
		}
		String keys   = "";
		ArrayList<Object> values = new ArrayList<Object>();
		for (int i = 0; i < (value.size()/2); i++) {
			keys   += value.get(i*2).toString() + " = ?,";
			values.add(value.get(i*2+1));
		}
		keys     = keys.substring(0, keys.length() - 1);
		Object[] bindArgs = (Object[]) values.toArray(new Object[value.size()/2]);
		try {
			//更新方法，1、table 2、ContentValues 3、user_name=? 4、new String[]{user_name}
//			sqLiteDatabase.update(table, values, whereClause, whereArgs)
			String sql = "UPDATE "+ table +" SET "+ keys +" WHERE 1=1";
			if ( whereClause!=null && !whereClause.equals("") ) {
				sql += " AND "+whereClause;
			}
			mSqliteDatabase.execSQL(sql, bindArgs);
			return true;
		} catch (SQLiteException e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 删除数据方法
	 * 
	 * @param table       表名
	 * @param whereClause 删除条件，如： user_name = "lujun"。没有条件是为null或""
	 */
	public boolean delete(String table, String whereClause){
		// XXX 2014-08-07
		try {
			//删除方法，1、table 2、user_cId=? 3、new String[]{user_cId},SQL中问号的占位符
//			sqLiteDatabase.delete(table, whereClause, whereArgs);
			String sql = "DELETE FROM "+ table +" WHERE 1=1";
			if ( whereClause!=null && !whereClause.equals("") ) {
				sql += " AND "+whereClause;
			}
			mSqliteDatabase.execSQL(sql);
			return true;
		} catch (SQLiteException e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 查询方法
	 * 
	 * @param table       表名
	 * @param columnName  列名称。列名必须为数据库表中存在的,若是查询所有信息，此参数为：（*）shift+8；查询多列请用逗号隔开
	 * @param whereClause 查询条件，如：user_name="lucy"。没有条件则为null或""
	 * @param orderWhat   排序内容，列名。不排序为null或""
	 * @param orderWay    排序方式。-1表示不排序、0表示升序、1表示降序
	 * @param limitCase   限制条件，如：限制查询为三条记录则为 3，无限制条件则为-1
	 * 
	 * @return 查询成功返回Cuesor
	 * 
	 * Cursor 遍历方式
	 * while(cursor.moveToNext()){
			for (int i = 0; i < cursor.getColumnCount(); i++) {
				if (cursor.getString( cursor.getColumnIndex( cursor.getColumnName(i) ) ) == null) {
					//遍历得到的结果为空
				}else {
					//结果cursor.getString( cursor.getColumnIndex( cursor.getColumnName(i) ) )
				}
			}
		}
	 */
	public Cursor query(String table, String columnName, String whereClause, String orderWhat, int orderWay, int limitCase){
		// XXX 2014-08-07
		try {
			String sql = "SELECT " + columnName + " FROM "+ table + " WHERE 1=1";
			if ( whereClause!=null && !whereClause.equals("") ) {
				sql += " AND "+whereClause;
			}
			if (!(orderWay == -1) && orderWhat!=null && !orderWhat.equals("")) {
				switch (orderWay) {
				case 0:
					sql += " ORDER BY " + orderWhat;
					break;

				case 1:
					sql += " ORDER BY " + orderWhat + " DESC";
					break;
					
				default:
					break;
				}
			}
			if (!(limitCase == -1)) {
				sql += " LIMIT "+ limitCase;
			}
			Cursor cursor = mSqliteDatabase.rawQuery(sql, null);
			return cursor;
		} catch (SQLiteException e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 关闭数据库连接
	 * 
	 * 在Activity中重写onDestroy()方法中调用即可
	 */
	public void onCloseDB(){
		mSqliteDatabase.close();
	}
}
