package com.wdl.myclass;

public class Constant {
	
	/**
	 * 请求URL
	 */
	public static final String SERVER_REG_URL = "http://114.215.169.41/wandianle/signup/signup.php";
	public static final String SERVER_LOGINVAR_URL = "http://114.215.169.41/wandianle/login/login.php";
	public static final String SERVER_USER_AVATAR_URL = "http://114.215.169.41/wandianle/headimg/";
	public static final String SERVER_USER_INFO_URL = "http://114.215.169.41/wandianle/apply/apply.php";
	public static final String SERVER_SHARE_URL = "http://114.215.169.41/wandianle/usershare/share.php";
	public static final String SERVER_CON_URL = "http://114.215.169.41/wandianle/showlist/showlist.php";
	public static final String SERVER_UPLOAD_AVATAR = "http://114.215.169.41/wandianle/changeimg/changeimg.php";
	public static final String SERVER_MODIFY_INFO_URL = "http://114.215.169.41/wandianle/minfo/minfo.php";
	public static final String SERVER_MAP_URL = "http://114.215.169.41/wandianle/map/map.php";
	public static final String SERVER_UD_URL = "http://114.215.169.41/wandianle/updown/updown.php";
	public static final String SERVER_SUG_URL = "http://114.215.169.41/wandianle/reply/reply.php";
	
	public static final String DEFAULT_HTTP_HEAD = "http://";
	
	
	/**
	 * 数据库
	 */
	public static final String DB_NAME    = "wandianle.db";                       //数据库名称
	public static final int    DB_VERSION = 1;                                   //数据库版本号
	
	/**
	 * 数据表
	 */
	public static final String ID = "id";
	
	public static final String TABLE_USER = "user";
	public static final String TABLE_USER_NAME = "username";
	public static final String TABLE_USER_NICKNAME = "nickname";
	public static final String TABLE_USER_PASSWORD = "password";
	public static final String TABLE_USER_SINCON = "sincon";
	public static final String TABLE_USER_SEX = "sex";
	public static final String TABLE_USER_HEADURL = "headurl";
	public static final String TABLE_USER_PHONE = "phone";
	public static final String TABLE_USER_SCORE = "score";
	public static final String TABLE_USER_CANSHARE = "canshare";
	
	
	/**
	 * JSON解析
	 */
	public static final String JSON_RESULT_HEAD = "res";
	public static final String JSON_TOTAL_HEAD = "total";
	public static final String JSON_CON_HEAD = "userinfo";
	public static final String [] JSON_CON_KEYS = new String[]{
																"id","username","which","note","time",
																"longitude","latitude","location","timelater","up",
																"down"
															  };
	
	
	
	/** 消息返回类型
	 *  0--HTTP请求返回
	 *  1--文件IO返回
	 *  2--数据库返回
	 *  3--定位返回
	 *  4--POI返回
	 */
	public static final int [] MSG_WHAT = new int[]{0, 1, 2};
	
	public static final String[] POI_NAME = new String[]{"", "东", "南", "西", "北", "站"};
	public static final int POT_NUM = 10;
	public static final int TABLE_ID_KEY = 1;
	public static final int LAUNCHER_DELAY_TIME = 2300;
	public static final String USER_IMG_PATH = "/user/images/";
	public static final String USER_AVATAR_NAME = "avatar.png";
	public static final String USER_TEMP_CACHE_AVATARS = "/tmpcache/images/";
	
	public static final String TRAINNO_KEY = "trainno";
	public static final int ONE_PAGE_NUM = 10;
}
