package com.wdl.view;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.wdl.myclass.Constant;
import com.wdl.myclass.HttpOperation;
import com.wdl.myclass.MD5;
import com.wdl.myclass.NetWorkInfo;
import com.wdl.myclass.SqliteUtil;
import com.wdl.myclass.SystemSet;
import com.wdl.myclass.UsualTools;
import com.wdl.wandianle.MainActivity;
import com.wdl.wandianle.R;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class LoginActivity extends Activity {
	private ActionBar mActionBar;
	private Button btnLoginSubmit;
	private Button btnLoginFpwd;
	private EditText etLoginUserName;
	private EditText etLoginPassword;
	private ProgressDialog mProgressDialog;
	private LinearLayout llLogin;
	
	private Animation mAnimation;
	private JSONObject mJsonObject;
	private Intent mIntent;
	private Cursor mCursor;
	private ArrayList<NameValuePair> mList;
	private SqliteUtil mSqliteUtil;
	private ArrayList<Object> mArrayList;
	private HttpOperation mHttpOperation;
	private SystemSet mSystemSet;
	private Handler mHandler = new Handler(){
		
		@Override
		public void handleMessage(Message msg){
			super.handleMessage(msg);
			switch (msg.what) {
			case 0:
				dealResult(mHttpOperation.mResult);
				break;

			default:
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		init();
	}

	private void init(){
		mActionBar = this.getActionBar();
		mActionBar.hide();
		
		btnLoginFpwd = (Button) this.findViewById(R.id.btn_login_fpwd);
		btnLoginSubmit = (Button) this.findViewById(R.id.btn_login_submit);
		etLoginUserName = (EditText) this.findViewById(R.id.login_username);
		etLoginPassword = (EditText) this.findViewById(R.id.login_password);
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage(getResources().getString(R.string.please_wait));
		llLogin = (LinearLayout) this.findViewById(R.id.linearlayout_login);
		
		btnLoginFpwd.setOnClickListener(new BtnOnClickListener());
		btnLoginSubmit.setOnClickListener(new BtnOnClickListener());
		
		mList = new ArrayList<NameValuePair>();
		mArrayList = new ArrayList<Object>();
		mSqliteUtil = new SqliteUtil(LoginActivity.this, null);
		
		mAnimation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.loginandreg_anim);
		mAnimation.setAnimationListener(new OnAnimationListener());
		llLogin.setAnimation(mAnimation);
	}
	
	private class BtnOnClickListener implements OnClickListener{
		
		@Override
		public void onClick(View v){
			switch (Integer.parseInt(v.getTag().toString())) {
			case 0://忘记密码
				UsualTools.goBowser(LoginActivity.this, Constant.DEFAULT_HTTP_HEAD);
				break;
			
			case 1://登录
				if (!NetWorkInfo.isNetWorkConnected(LoginActivity.this)) {
					Toast.makeText(LoginActivity.this, getResources().getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
					return;
				}
				try {
					mProgressDialog.show();
					mHttpOperation = new HttpOperation(1, Constant.SERVER_LOGINVAR_URL, "", mHandler);
					mList.add(new BasicNameValuePair("username", etLoginUserName.getText().toString()));
					mList.add(new BasicNameValuePair("password", MD5.getMD5(etLoginPassword.getText().toString())));
					mHttpOperation.setHttpRequest(mList);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;

			default:
				break;
			}
		}
	}
	
	private void dealResult(String result){
		mProgressDialog.dismiss();
		try {
			mJsonObject = new JSONObject(result);
			if (mJsonObject.getString(Constant.JSON_RESULT_HEAD) == null  || mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("")) {
				Toast.makeText(LoginActivity.this, getResources().getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
				return;
			}
			if (mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("1110")) {
				Toast.makeText(LoginActivity.this, getResources().getString(R.string.msg_var_error), Toast.LENGTH_SHORT).show();
				return;
			}
			try {
				mArrayList.add(Constant.TABLE_USER_NAME);
				mArrayList.add(etLoginUserName.getText().toString());
				mArrayList.add(Constant.TABLE_USER_PASSWORD);
				mArrayList.add(MD5.getMD5(etLoginPassword.getText().toString()));
				mArrayList.add(Constant.TABLE_USER_NICKNAME);
				mArrayList.add("");
				mArrayList.add(Constant.TABLE_USER_SINCON);
				mArrayList.add("");
				mArrayList.add(Constant.TABLE_USER_SEX);
				mArrayList.add("");
				mArrayList.add(Constant.TABLE_USER_HEADURL);
				mArrayList.add("");
				mArrayList.add(Constant.TABLE_USER_PHONE);
				mArrayList.add("");
				mArrayList.add(Constant.TABLE_USER_SCORE);
				mArrayList.add("");
				mArrayList.add(Constant.TABLE_USER_CANSHARE);
				mArrayList.add("");
				
				mSqliteUtil = new SqliteUtil(LoginActivity.this, null);
				mCursor = mSqliteUtil.query(Constant.TABLE_USER, "*", Constant.ID + "=" + Constant.TABLE_ID_KEY, "", -1, -1);
				if (mCursor.getCount() == 0) {
					mCursor.close();
					if (!mSqliteUtil.insert(Constant.TABLE_USER, mArrayList)) {
						return;
					}
				}else {
					mCursor.close();
					if (!mSqliteUtil.update(Constant.TABLE_USER, mArrayList, Constant.ID + "=" + Constant.TABLE_ID_KEY)) {
						return;
					}
				}
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mIntent = new Intent(LoginActivity.this, MainActivity.class);
			startActivity(mIntent);
			finish();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(LoginActivity.this, getResources().getString(R.string.msg_parse_json_error), Toast.LENGTH_SHORT).show();
			return;
		}
	}
	
	private class OnAnimationListener implements AnimationListener{

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			// TODO Auto-generated method stub
			etLoginUserName.requestFocus();
			mSystemSet = new SystemSet(LoginActivity.this);
			mSystemSet.showOrHideInputMethodManager();
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
		}
	}
}
