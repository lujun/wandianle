package com.wdl.modle;

import java.util.ArrayList;
import java.util.List;

import android.os.Handler;

import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiCitySearchOption;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapapi.search.poi.PoiSearch;

public class PoiSearchClass {
	/** 百度LBS*/
	private PoiSearch mPoiSearch;//POI检索接口
	private String mCity;//当前检索城市
	private String mKeyWord;//检索关键字
	private ArrayList<String> mPoiName;//返回结果数组
	private List<PoiInfo> mResuList; //返回结果集
	private Handler mHandler;
	private int rNum;//需要检索结果数量

	public PoiSearchClass(PoiSearch poiSearch, String city, String keyword, int num, Handler handler) {
		// TODO Auto-generated constructor stub
		this.mPoiSearch = poiSearch;
		this.mCity = city;
		this.mKeyWord = keyword;
		this.rNum = num;
		this.mHandler = handler;
		this.mPoiName = new ArrayList<String>();
		this.mResuList = new ArrayList<PoiInfo>();
		initPoiSearch();
	}
	
	private void initPoiSearch(){
		mPoiSearch.setOnGetPoiSearchResultListener(new PoiListener());
	}
	
	private class PoiListener implements OnGetPoiSearchResultListener{
		//获取POI检索结果
		@Override
		public void onGetPoiResult(PoiResult poiResult){
			if (poiResult == null || poiResult.error == SearchResult.ERRORNO.RESULT_NOT_FOUND) {
				//
			}
			if (poiResult.error == SearchResult.ERRORNO.NO_ERROR) {
				mResuList = poiResult.getAllPoi();
				for (PoiInfo poiObj : mResuList) {
					mPoiName.add(poiObj.name);
				}
			}
			if (poiResult.error == SearchResult.ERRORNO.AMBIGUOUS_KEYWORD) {
				// 当输入关键字在本市没有找到，但在其他城市找到时，返回包含该关键字信息的城市列表
				//
			}
			mHandler.sendEmptyMessage(4);
		}
		
		//获取Place详情页检索结果
		@Override
		public void onGetPoiDetailResult(PoiDetailResult poiDetailResult){
			if (poiDetailResult.error == SearchResult.ERRORNO.NO_ERROR) {
				//TODO 详细信息解析
			}
		}
	}
	
	public void startPoiSearch(){
		mPoiSearch.searchInCity(new PoiCitySearchOption().city(mCity).keyword(mKeyWord).pageNum(rNum));//发起检索请求
	}
	
	public void releasePoiSearch(){
		mPoiSearch.destroy();
	}
	
	public ArrayList<String> getPoiName(){
		return mPoiName;
	}

}
