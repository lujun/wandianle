package com.wdl.view;

import java.io.File;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.baidu.location.LocationClientOption.LocationMode;
import com.wdl.modle.Location;
import com.wdl.modle.PoiSearchClass;
import com.wdl.myclass.Constant;
import com.wdl.myclass.FileOperation;
import com.wdl.myclass.HttpOperation;
import com.wdl.myclass.NetWorkInfo;
import com.wdl.myclass.SqliteUtil;
import com.wdl.myclass.UsualTools;
import com.wdl.wandianle.R;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ShareActivity extends Activity {
	/** Widget */
	private Button btnBack;
	private Button btnSave;
	private TextView tvLocation;
	private EditText etText;
	private EditText etTrainNo;
	private ImageView ivUserAvatar;
	
	private HttpOperation mHttpOperation;
	private SqliteUtil mSqliteUtil;
	private Cursor mCursor;
	private ArrayList<NameValuePair> mArrayList;
	private JSONObject mJsonObject;
	private String tmpPoiName;
	
	private Location mLocation;
	private PoiSearchClass mPoiSearchClass;
	private Handler mHandler = new Handler(){
		
		@Override
		public void handleMessage(Message msg){
			super.handleMessage(msg);
			switch (msg.what){
			case 0://http
				parseResult(mHttpOperation.mResult);
				break;
				
			case 3://定位返回
				setLocation();
				break;
				
			case 4://POI返回
				mPoiSearchClass.getPoiName().size();
				if (mPoiSearchClass.getPoiName().size() == 0) {
					return;
				}
				break;

			default:
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_share);
		init();
	}

	private void init() {
		ActionBar mActionBar = this.getActionBar();
		mActionBar.hide();
		btnBack = (Button) this.findViewById(R.id.back_share);
		btnSave = (Button) this.findViewById(R.id.btn_share_save);
		tvLocation = (TextView) this.findViewById(R.id.tv_share_location);
		etText = (EditText) this.findViewById(R.id.et_share_text);
		etTrainNo = (EditText) this.findViewById(R.id.et_share_trainno);
		ivUserAvatar = (ImageView) this.findViewById(R.id.share_user_avatar);

		btnBack.setOnClickListener(new BtnOnClickListener());
		btnSave.setOnClickListener(new BtnOnClickListener());
		
		mSqliteUtil = new SqliteUtil(this, null);
		mArrayList = new ArrayList<NameValuePair>();
		tmpPoiName = null;
		
		if (NetWorkInfo.isNetWorkConnected(ShareActivity.this)) {
			Toast.makeText(ShareActivity.this, getResources().getString(R.string.msg_neednot_open_gps), Toast.LENGTH_SHORT).show();
		}
		openGPS();
		
		File file = new File(FileOperation.getPackagePath(this, "").get(0) + Constant.USER_IMG_PATH + Constant.USER_AVATAR_NAME);
		if (!file.exists()) {
			Toast.makeText(ShareActivity.this, getResources().getString(R.string.msg_no_avatar), Toast.LENGTH_SHORT).show();
			return;
		}
		ivUserAvatar.setImageBitmap(UsualTools.toRoundCorner(UsualTools.readPic(FileOperation.getPackagePath(this, "").get(0) + Constant.USER_IMG_PATH + Constant.USER_AVATAR_NAME), 500));
		
	}

	private class BtnOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			switch (Integer.parseInt(v.getTag().toString())) {
			case 0:// 返回
				finish();
				break;

			case 1:// 保存
				if (etText.getText().equals("") || etTrainNo.getText().toString().equals("")) {
					Toast.makeText(ShareActivity.this, getResources().getString(R.string.msg_say_some), Toast.LENGTH_SHORT).show();
					return;
				}
				if (!NetWorkInfo.isNetWorkConnected(ShareActivity.this)) {
					Toast.makeText(ShareActivity.this, getResources().getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
					return;
				}
				mCursor = mSqliteUtil.query(Constant.TABLE_USER, "*", Constant.ID + "=" + Constant.TABLE_ID_KEY, "", -1, -1);
				if (mCursor.getCount() == 0) {
					Toast.makeText(ShareActivity.this, getResources().getString(R.string.msg_db_read_error), Toast.LENGTH_SHORT).show();
					mCursor.close();
					return;
				}
				mCursor.moveToFirst();
				if (mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NAME)).equals("")
						||  mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_PASSWORD)).equals("")
						||  mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NAME)) == null
						||  mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_PASSWORD)) == null) {
					Toast.makeText(ShareActivity.this, getResources().getString(R.string.msg_db_read_error), Toast.LENGTH_SHORT).show();
					mCursor.close();
					return;
				}
				if (mLocation.getLatitude() == 0 || mLocation.getAddress().equals("") || mLocation.getAddress() == null || tmpPoiName.equals("") || tmpPoiName == null) {
					Toast.makeText(ShareActivity.this, getResources().getString(R.string.msg_locationing), Toast.LENGTH_SHORT).show();
					mCursor.close();
					return;
				}
				mArrayList.add(new BasicNameValuePair("username", mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NAME))));
				mArrayList.add(new BasicNameValuePair("password", mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_PASSWORD))));
				mArrayList.add(new BasicNameValuePair("note", etText.getText().toString()));
				mArrayList.add(new BasicNameValuePair("which", etTrainNo.getText().toString()));
				mArrayList.add(new BasicNameValuePair("longitude", Double.toString(mLocation.getLongittude())));
				mArrayList.add(new BasicNameValuePair("latitude", Double.toString(mLocation.getLatitude())));
				mArrayList.add(new BasicNameValuePair("location", mLocation.getAddress()));
				mArrayList.add(new BasicNameValuePair("stations", tmpPoiName));
				mHttpOperation = new HttpOperation(1, Constant.SERVER_SHARE_URL, "", mHandler);
				mHttpOperation.setHttpRequest(mArrayList);
				etTrainNo.setText("");
				etText.setText("");
				etTrainNo.requestFocus();
				Toast.makeText(ShareActivity.this, getResources().getString(R.string.msg_have_send), Toast.LENGTH_SHORT).show();
				mCursor.close();
				break;
				
			case 2://重新定位
				tvLocation.setText(getResources().getString(R.string.hint_share_locationing));
				mLocation.stopLocation();
				mLocation.startLocation();
				break;

			default:
				break;
			}
		}
	}
	
	private void setLocation(){
		if (mLocation.getAddress() == null || mLocation.getAddress().equals("") || mLocation.getCity() == null || mLocation.getCity().equals("")) {
			tvLocation.setText(getResources().getString(R.string.hint_share_location_failed));
			tvLocation.setOnClickListener(new BtnOnClickListener());
		} else if (!mLocation.getAddress().equals(getResources().getString(R.string.hint_share_location_failed))) {
			tvLocation.setText(mLocation.getAddress());
			if (mLocation.getDistrict() == null || mLocation.getDistrict().equals("")) {
				newPoiSearch(new String[]{mLocation.getCity()});
			}else {
				newPoiSearch(new String[]{mLocation.getDistrict(), mLocation.getCity()});
			}
		}
	}

	private void openGPS(){
		if (!UsualTools.hasGPSDevice(ShareActivity.this)) {
			newLocationObject();
			return;
		}
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			new AlertDialog.Builder(this)
			.setCancelable(false)
			.setTitle(getResources().getString(R.string.hint_warm))
			.setMessage(getResources().getString(R.string.GPS_hint))
			.setPositiveButton(getResources().getString(R.string.yes),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent();
							intent.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							try {
								startActivity(intent);
								newLocationObject();
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
						}
					})
			.setNegativeButton(getResources().getString(R.string.no),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							newLocationObject();
						}
					}).show();
		}
	}
	
	private void newLocationObject(){
		mLocation = new Location(getApplicationContext(), mHandler, LocationMode.Hight_Accuracy, "gcj02", 5000);
		mLocation.startLocation();
	}
	
	private void newPoiSearch(String... cities){
		tmpPoiName = "";
		for (int i = 0; i < Constant.POI_NAME.length; i++) {
			if (cities.length > 1) {
				tmpPoiName += cities[0] + Constant.POI_NAME[i] + ";" + cities[0].substring(0, cities[1].length()-1) + Constant.POI_NAME[i] + ";" + cities[1].substring(0, cities[1].length()-1) + Constant.POI_NAME[i] + ";";
			}else {
				tmpPoiName += cities[0].substring(0, cities[0].length()-1) + Constant.POI_NAME[i] + ";";
			}
		}
		tmpPoiName = tmpPoiName.substring(0, tmpPoiName.length()-1);
		//POI
		/*SDKInitializer.initialize(getApplicationContext());	
		mPoiSearchClass = new PoiSearchClass(PoiSearch.newInstance(), cities[0], Constant.POI_NAME[0], Constant.POT_NUM, mHandler);
		mPoiSearchClass.startPoiSearch();*/
	}
	
	private void parseResult(String result){
		try {
			mJsonObject = new JSONObject(mHttpOperation.mResult);
			if (mJsonObject.getString("res") == null || mJsonObject.getString("res").equals("")) {
				Toast.makeText(this, getResources().getString(R.string.msg_send_error), Toast.LENGTH_SHORT).show();
			}else if (mJsonObject.getString("res").equals("2220")) {
				Toast.makeText(this, getResources().getString(R.string.msg_send_error), Toast.LENGTH_SHORT).show();
			}else if (mJsonObject.getString("res").equals("2221")) {
				Toast.makeText(this, getResources().getString(R.string.msg_send_succeed), Toast.LENGTH_SHORT).show();
			}else if (mJsonObject.getString("res").equals("2222")) {
				Toast.makeText(this, getResources().getString(R.string.msg_send_wrong_info), Toast.LENGTH_SHORT).show();
			}else {
				Toast.makeText(this, getResources().getString(R.string.msg_send_error), Toast.LENGTH_SHORT).show();
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onStop() {
		if (mLocation != null) {
			mLocation.stopLocation();
		}
		super.onStop();
	}
	
	@Override
	public void onDestroy(){
//		mPoiSearchClass.releasePoiSearch();//POI释放
		super.onDestroy();
	}
}
