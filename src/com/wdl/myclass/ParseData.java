package com.wdl.myclass;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.Context;

/**
 * 数据解析类（包括普通数据、json数据、xml数据）
 * 
 * @author lujun
 * @since  2014-08-10
 *
 */
public class ParseData {
	/** 成员变量声明*/
	private JSONTokener mJsonTokener; //Json解析实例
	private JSONObject  mJsonObject;  //Json对象
	private JSONArray   mJsonArray;   //Json数组

	/**
	 * 普通数据解析
	 * 
	 * @param context      上下文，如果所需的数据需要从文件读取，此参数必填。否则为null。
	 * @param isFromFile   是否从读取文件解析数据,true是。
	 * @param where        若是从文件读取数据。0代表手机存存储，1代表SD卡存储。否则为-1
	 * @param relativePath 若需要解析的数据从文件读取，此处为文件的相对路径。否则为null或为""。如"/test/test.txt"
	 * @param parseStr     若需要解析的字符串不是从文件读取，此处为需要解析的字符串。否则为null或者""。
	 * @param regRule      正则规则
	 * 
	 * @return             String实例。
	 */
	public String parseGeneralData(Context context, boolean isFromFile, int where, String relativePath, String parseStr, String regRule){
		// FIXME 2014-08-11
		if (isFromFile) {
			String pathName = "/";
			for (int i = 0; i < relativePath.split("/").length-1; i++) {
				pathName += relativePath.split("/")[i] + "/";
			}
			parseStr = FileOperation.readFile(context, "", pathName, relativePath.split("/")[relativePath.split("/").length-1], where, null);
		}
		Pattern mPattern = Pattern.compile(regRule, Pattern.CASE_INSENSITIVE);    //构建表达法
		Matcher mMatcher = mPattern.matcher(parseStr);                            //表达法匹配字符串
		if (!mMatcher.find()) {
			return null;
		}
		return mMatcher.group(mMatcher.groupCount());
	}
	
	/**
	 * Json解析
	 * 
	 * @param jsonName   json对象的键名称，如下面的people
	 * @param jsonObject 需要解析的json字符串数组，如"{"people":[{"name":"lujun","age":"21","sex":"M"},{"name":"lujun","age":"21","sex":"M"}]}"
	 * @param keys       解析的json的key数组，如{"name","age","sex"}
	 * 
	 * @return           ArrayList<ArrayList<String>>实例。
	 */
	public ArrayList<ArrayList<String>> parseJsonData(String jsonName, String jsonObject, String[] keys){
		// XXX 2014-08-11
		ArrayList<ArrayList<String>> mArrayLists = new ArrayList<ArrayList<String>>();
		try {
			mJsonTokener = new JSONTokener(jsonObject);
//			while((mJsonObject = (JSONObject)mJsonTokener.nextValue()) != null){//此处while读取时读取到末尾时因为已经读取完毕，
																				//但是还没有赋值操作，会有异常抛出
																				//可不用while，直接用一句mJsonObject = (JSONObject)mJsonTokener.nextValue()
				mJsonObject = (JSONObject)mJsonTokener.nextValue();
				if( (mJsonArray = (JSONArray)mJsonObject.get(jsonName) ) != null){
					for (int i = 0; i < mJsonArray.length(); i++) {
						ArrayList<String> mArrayList = new ArrayList<String>();
						for (String key : keys) {
							mArrayList.add(mJsonArray.getJSONObject(i).getString(key));
						}
						mArrayLists.add(mArrayList);
					}
					return mArrayLists;
				}
				return null;
//			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * XML解析
	 * 
	 * @param xmlObject 需要解析的xml字符串对象
	 * 
	 * @return ArrayList<ArrayList<String>>实例
	 */
	public ArrayList<ArrayList<String>> parseXmlDada(String xmlObject){
		// FIXME 2014-08-11
		ArrayList<ArrayList<String>> mArrayLists = new ArrayList<ArrayList<String>>();
		
		return mArrayLists;
	}
	
	/**
	 * json数组解析成字符串
	 * @param arr
	 * @return String
	 */
	public String parseJSONArraytoString(JSONArray arr) {
		StringBuffer str = new StringBuffer();
		for (int i = 0; i < arr.length(); i++) {
			try {
				str = str.append(arr.getString(i)).append(" ");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str.toString();
	}
}
