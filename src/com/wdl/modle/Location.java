package com.wdl.modle;

import android.content.Context;
import android.os.Handler;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.wdl.wandianle.R;

public class Location {
	/** 百度LBS*/
	private LocationClient mLocationClient;                                   //定位服务的客户端
	private LocationClientOption mLocationClientOption;                       //定位服务客户端设置类
	private LocationMode mLocationMode;                                       //定位模式
	private String tmpCoor;                                                   //返回的定位结果类型
	private int mSpan;                                                        //发起定位请求的时间间隔
	private MyLocationListener mMyLocationListener;                           //监听
	/** members*/
	private Context mContext;
	private Handler mHandler;
	/** Location详细信息*/
	private String address; // 详细地理位置信息
	private String province; // 省
	private String city; // 城市
	private String district; // 区/县
	private String street; // 街道信息
	private String streetNo; // 街道号码
	private String time; // server返回的当前定位时间
	private boolean hasAddr; // 是否有地址信息
	private double latitude; // 纬度坐标
	private double longittude; // 经度坐标
	private float direation; // 手机当前方向 ，范围【0-360】，手机上部正朝向北的方向为0°方向
	private float radious;   //所用坐标系
	private float speed; //获取速度，仅gps定位结果时有速度信息
//	private int locType; // 定位类型
	private int errorCode; // 错误信息
	private int satelliteNumber; //gps定位结果时，获取gps锁定用的卫星数
	private int operator;  //运营商信息
	/*
	 * 61 ： GPS定位结果 
	 * 62 ： 扫描整合定位依据失败。此时定位结果无效。 
	 * 63 ：网络异常，没有成功向服务器发起请求。此时定位结果无效。 
	 * 65 ： 定位缓存的结果。 
	 * 66 ：离线定位结果。通过requestOfflineLocaiton调用时对应的返回结果 
	 * 67 ：离线定位失败。通过requestOfflineLocaiton调用时对应的返回结果 
	 * 68 ： 网络连接失败时，查找本地离线定位时对应的返回结果
	 * 161： 表示网络定位结果 
	 * 162~167： 服务端定位失败 
	 * 502：key参数错误 
	 * 505：key不存在或者非法
	 * 601：key服务被开发者自己禁用 
	 * 602：key mcode不匹配 
	 * 501～700：key验证失败
	 */

	/**
	 * Construct method
	 * 
	 * @param context        应用程序上下文
	 * @param locationMode   定位模式
	 * @param coor           数据类型
	 * @param span           发起定位请求的间隔时间
	 */
	public Location(Context context, Handler handler, LocationMode locationMode,String coor, int span) {
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.mHandler = handler;
		mLocationMode = locationMode;
		this.tmpCoor = coor;
		this.mSpan = span;
		mLocationClient = new LocationClient(this.mContext);
		mMyLocationListener = new MyLocationListener();
		mLocationClient.registerLocationListener(mMyLocationListener);      //注册监听函数,用于发起网络请求
	}

	public void startLocation() {
		initLocation();
		mLocationClient.start();
	}

	public void stopLocation() {
		mLocationClient.stop();
	}
	
	private void initLocation(){
		mLocationClientOption = new LocationClientOption();
		mLocationClientOption.setLocationMode(mLocationMode);   //设置模式
		mLocationClientOption.setCoorType(tmpCoor);             //设置返回类型
		mLocationClientOption.setScanSpan(mSpan);               //设置发起定位请求的时间间隔
		mLocationClientOption.setIsNeedAddress(true);           //设置需要返回地址信息
		mLocationClientOption.setOpenGps(true);                 //设置打开GPS定位
		mLocationClientOption.setNeedDeviceDirect(true);        //设置需要返回结果包含手机头的方向
		mLocationClient.setLocOption(mLocationClientOption);
	}

	private class MyLocationListener implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			time = location.getTime();
			errorCode = location.getLocType();
			latitude = location.getLatitude();
			longittude = location.getLongitude();
			radious = location.getRadius();
			if (location.getLocType() == BDLocation.TypeGpsLocation) {
				speed = location.getSpeed();
				satelliteNumber = location.getSatelliteNumber();
				direation = location.getDirection();
				address = location.getAddrStr();
			} else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {
				address = location.getAddrStr();
				operator = location.getOperators();
				province = location.getProvince();
				city = location.getCity();
				district = location.getDistrict();
				street = location.getStreet();
				streetNo = location.getStreetNumber();
			} else if (location.getLocType() == BDLocation.TypeOffLineLocation) {
				//离线定位结果
			} else if (location.getLocType() == BDLocation.TypeOffLineLocationNetworkFail) {
				//网络请求失败,基站离线定位结果
			} else if (location.getLocType() == BDLocation.TypeServerError
						|| location.getLocType() == BDLocation.TypeOffLineLocationFail
						|| location.getLocType() == BDLocation.TypeNone
						|| location.getLocType() == BDLocation.TypeNetWorkException) {
				//   server定位失败，没有对应的位置信息
				// ||离线定位失败
				// ||无效定位结果
				// ||网络连接失败
				address = mContext.getResources().getString(R.string.hint_share_location_failed);
				
			}
			mHandler.sendEmptyMessage(3);
		}
	}
	
	public String getAddress(){
		return address;
	}
	
	public String getProvince(){
		return province;
	}
	
	public String getCity(){
		return city;
	}
	
	public String getDistrict(){
		return district;
	}
	
	public String getStreet(){
		return street;
	}
	
	public double getLatitude(){
		return latitude;
	}
	
	public double getLongittude(){
		return longittude;
	}
	
	public int getErrorCode(){
		return errorCode;
	}
}
