package com.wdl.myclass;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;

/**
 * FileOperation类，获取SD卡或手机存储状态以及相关信息，读、写SD卡或手机存储操作
 * 
 * @author lujun
 * @since  2014-08-04
 *
 */
public class FileOperation {
	
	/**
	 * 获取SD卡状态是否挂起
	 * 
	 * @return true 可以使用
	 */
	public static boolean isSDCardHangUp(){
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 写文件
	 * 
	 * @param context  程序所在的activity上下文
	 * @param db_name  需要的数据库名称
	 * @param pathName 相对路径，如 /text/res/
	 * @param fileName 文件名，如test.txt
	 * @param text     要写入的内容
	 * @param where    写的位置。0代表手机内存，1代表SD卡
	 * @param handler  消息处理示例，操作完成后返回消息what值为1。不需要消息返回为null
	 * 
	 * @return boolean 成功true
	 */
	public static boolean writeFile(Context context,String db_name, String pathName, 
											String fileName, String text, int where, Handler handler){
		String outPath = null;
		switch (where) {
		case 0:
			outPath = getPackagePath(context, db_name).get(0);
			break;

		case 1:
			if (!isSDCardHangUp()) {
				return false;
			}
			outPath = Environment.getExternalStorageDirectory() + "";
			break;
		}
		try {
			/** 开启新的线程用于写文件*/
			MyThread myThread = new MyThread(context, outPath, db_name, pathName, fileName, text, where, 0, handler);
			Thread thread = new Thread(myThread);
			thread.start();
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 读文件
	 * 
	 * @param context  程序所在的activity上下文
	 * @param db_name  需要的数据库名称
	 * @param pathName 相对路径，如 /text/res/
	 * @param fileName 文件名，如test.txt
	 * @param where    读的位置。0代表手机内存，1代表SD卡
	 * @param handler  消息处理示例，操作完成后返回消息what值为1。不需要消息返回为null
	 * 
	 * @return String类型的字符串
	 */
	public static String readFile(Context context,String db_name, 
											String pathName, String fileName, int where,Handler handler){
		String outPath = null;
		switch (where) {
		case 0:
			outPath = getPackagePath(context, db_name).get(0);
			break;

		case 1:
			if (!isSDCardHangUp()) {
				return null;
			}
			outPath = Environment.getExternalStorageDirectory() + "";
			break;
		}
		
		try {
			File path = new File(outPath + pathName);
			if (!path.exists()) {
				return null;
			}
			File file = new File(outPath + pathName + fileName);
			if (!file.exists()) {
				return null;
			}
			/** 开启新的线程用于读文件*/
			MyThread myThread = new MyThread(context, outPath, db_name, pathName, fileName, "", where, 1, handler);
			Thread thread = new Thread(myThread);
			thread.start();
			while(myThread.returnText == null){}
			return myThread.returnText;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 获取当前程序所在的相关路径
	 * 
	 * @param context 应用程序所在上下文，生命周期与该activity相同
	 * @param db_name  数据库名称
	 * 
	 * @return 当前程序路径ArrayList
	 * 		   1、当前程序路径,files目录下
	 * 		   2、程序安装包的路径
	 * 		   3、程序缓存路径
	 * 		   4、程序数据库的路径
	 */
	public static ArrayList<String> getPackagePath(Context context, String db_name){
		ArrayList<String> pathArrayList = new ArrayList<String>();
		//当前程序的路径
		pathArrayList.add(context.getApplicationContext().getFilesDir().getAbsolutePath());
		//当前程序安装包的路径
		pathArrayList.add(context.getApplicationContext().getPackageResourcePath());
		//当前程序缓存路径
		pathArrayList.add(context.getApplicationContext().getCacheDir().toString());
		//当前程序数据库的路径
		if (!db_name.equals("")) {
			pathArrayList.add(context.getApplicationContext().getDatabasePath(db_name).getAbsolutePath());
		}
		return pathArrayList;
	}
	
	/**
	 * 读写文件线程
	 * 
	 * @author lujun
	 * @since  2014-08-05
	 *
	 */
	public static class MyThread implements Runnable{
		private static final int IS_DONE =1;
		public Context context;
		public String outPath, db_name, pathName, fileName, text, returnText;
		public Handler mHandler;
		/** 
		 * optFlag:0 refers to write
		 *         1 refers to read 
		 */
		public int where, optFlag;
		
		MyThread(Context context,String outPath, String db_name, String pathName, 
				String fileName, String text, int where, int optFlag, Handler handler) {
			// TODO Auto-generated constructor stub
			this.context    = context;
			this.outPath    = outPath;
			this.db_name    = db_name;
			this.pathName   = pathName;
			this.fileName   = fileName;
			this.text       = text;
			this.where      = where;
			this.optFlag    = optFlag;
			this.returnText = null;
			this.mHandler   = (handler==null)? null:handler;
		}
		
		@Override
		public void run(){
			switch (optFlag) {
			case 0:
				try {
					File path = new File(outPath + pathName);
					File file = new File(outPath + pathName + fileName);
					if (!path.exists()) {
						path.mkdirs();
					}
					if (!file.exists()) {
						file.createNewFile();
					}
					FileOutputStream fileOutputStream = new FileOutputStream(file);
					byte[] buffer = text.getBytes();
					fileOutputStream.write(buffer);
					fileOutputStream.flush();
					fileOutputStream.close();
					if (mHandler!=null) {
						mHandler.sendEmptyMessage(IS_DONE);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
				
			case 1:
				try {
					File file = new File(outPath + pathName + fileName);
					FileInputStream fileInputStream = new FileInputStream(file);
					/** 将指定的输入流包装成BufferedReader*/
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
					StringBuilder stringBuilder = new StringBuilder("");
					String line = null;
					while((line=bufferedReader.readLine()) != null){
						stringBuilder.append(line);
					}
					fileInputStream.close();
					returnText = stringBuilder.toString();
					if (mHandler!=null) {
						mHandler.sendEmptyMessage(IS_DONE);
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				break;

			default:
				break;
			}
		}
	}
	
	/**
	 * 删除文件或文件夹及其文件夹下的全部文件的方法
	 * 
	 * @param absolutePath 文件或文件夹的绝对路径
	 * @return 成功返回true
	 */
	public static boolean delete(String absolutePath){
		File mFile = new File(absolutePath);
		if (mFile.isFile()) {
			mFile.delete();
			return true;
		}
		if (mFile.isDirectory()) {
			File[] childFiles = mFile.listFiles();
			if (childFiles==null || childFiles.length==0) {
				mFile.delete();
				return true;
			}
			for (File file : childFiles) {
				delete(file.getAbsolutePath());
			}
			mFile.delete();
			return true;
		}
		return false;
	}
}