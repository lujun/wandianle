package com.wdl.view;

import com.wdl.myclass.Constant;
import com.wdl.myclass.SqliteUtil;
import com.wdl.wandianle.MainActivity;
import com.wdl.wandianle.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class LauncherActivity extends Activity {
	private static final int GOTO_DOOR_ACTIVITY = 91;
	private static final int GOTO_MAIN_ACTIVITY = 92;
	
	private SqliteUtil mSqliteUtil;
	private Cursor mCursor;
	private Intent mIntent;
	private Animation mAnimation;
	private ImageView launcherLogo;
	
	private ActionBar mActionBar;
	
	private Handler mHandler = new Handler(){
		
		@Override
		public void handleMessage(Message msg){
			super.handleMessage(msg);
			switch (msg.what) {
			case GOTO_DOOR_ACTIVITY:
				mIntent = new Intent(LauncherActivity.this, DoorActivity.class);
				startActivity(mIntent);
				finish();
				break;
				
			case GOTO_MAIN_ACTIVITY:
				mIntent = new Intent(LauncherActivity.this, MainActivity.class);
				startActivity(mIntent);
				finish();
				break;

			default:
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_launcher);
		init();
	}

	private void init() {
		mActionBar = this.getActionBar();
		mActionBar.hide();
		launcherLogo = (ImageView) this.findViewById(R.id.launcher_logo);
		mAnimation = AnimationUtils.loadAnimation(LauncherActivity.this, R.anim.launcher_anim);
		mAnimation.setAnimationListener(new OnAnimationListener());
		launcherLogo.setAnimation(mAnimation);
		
		mSqliteUtil = new SqliteUtil(LauncherActivity.this, null);
		mCursor = mSqliteUtil.query(Constant.TABLE_USER, "*", Constant.ID + "=" + Constant.TABLE_ID_KEY, "", -1, -1);
		if (mCursor.getCount() == 0) {
			mHandler.sendEmptyMessageDelayed(GOTO_DOOR_ACTIVITY, Constant.LAUNCHER_DELAY_TIME);
			mCursor.close();
			return;
		}
		mCursor.moveToFirst();
		if (mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NAME)).equals("") || mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_PASSWORD)).equals("")) {
			mHandler.sendEmptyMessageDelayed(GOTO_DOOR_ACTIVITY, Constant.LAUNCHER_DELAY_TIME);
			mCursor.close();
			return;
		}
		mHandler.sendEmptyMessageDelayed(GOTO_MAIN_ACTIVITY, Constant.LAUNCHER_DELAY_TIME);
		mCursor.close();
	}
	
	private class OnAnimationListener implements AnimationListener{

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
		}
	}
}
