package com.wdl.myclass;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

/**
 * sqlite数据库辅助类
 * 
 * @author lujun
 * @since 2014-08-04
 * 
 */
public class SqliteUtilHelper extends SQLiteOpenHelper {
	public SQLiteDatabase mSqliLiteDatabase;

	public SqliteUtilHelper(Context context, String name, CursorFactory factory,
			int version) {
		// TODO Auto-generated constructor stub
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		/** 建表操作，数据库第一次被创建时会被调用
		 * （在调用getReadableDatabase或getWritableDatabase时，
		 *   会判断构造函数中传过去的数据库是否存在,不存在才会调用此方法）
		 */
		/** 用户信息表*/
		db.execSQL("CREATE TABLE IF NOT EXISTS " + Constant.TABLE_USER + "(" 
																			+ Constant.ID +  " INTEGER PRIMARY KEY AUTOINCREMENT,"
																			+ Constant.TABLE_USER_NAME + " VARCHAR(20),"
																			+ Constant.TABLE_USER_NICKNAME + " VARCHAR(20),"
																			+ Constant.TABLE_USER_PASSWORD + " VARCHAR(128),"
																			+ Constant.TABLE_USER_SINCON + " TEXT,"
																			+ Constant.TABLE_USER_SEX + " VARCHAR(4),"
																			+ Constant.TABLE_USER_HEADURL + " VARCHAR(128),"
																			+ Constant.TABLE_USER_PHONE + " VARCHAR(20),"
																			+ Constant.TABLE_USER_SCORE + " VARCHAR(6),"
																			+ Constant.TABLE_USER_CANSHARE + " VARCHAR(1)"
																		+ ")");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		/** 如果发现数据库版本有变化，此方法会被调用
		 * （在调用getReadableDatabase或getWritableDatabase时，
		 *   会判断构造函数中传过去的数据库的版本是否比当前数据库版本新,新版本才会调用此方法）
		 */
	}
}
