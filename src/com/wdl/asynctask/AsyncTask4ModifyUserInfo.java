package com.wdl.asynctask;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.wdl.myclass.Constant;
import com.wdl.myclass.HttpOperation;
import com.wdl.myclass.SqliteUtil;
import com.wdl.wandianle.R;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.widget.Toast;

public class AsyncTask4ModifyUserInfo extends AsyncTask<String, Integer, Boolean> {
	private int mWhich;
	private String mText;
	private Context mContext;
	private SqliteUtil mSqliteUtil;
	private HttpOperation mHttpOperation;
	private Cursor mCursor;
	private ArrayList<NameValuePair> mArrayList;
	private ArrayList<Object> mArrayList4UpdateDb;
	private JSONObject mJsonObject;
	private boolean mPwdFlag;

	public AsyncTask4ModifyUserInfo(int which, String text, Context context) {
		this.mWhich = which;
		this.mText = text;
		this.mContext = context;
		this.mSqliteUtil = new SqliteUtil(mContext, null);
		this.mArrayList = new ArrayList<NameValuePair>();
		this.mArrayList4UpdateDb = new ArrayList<Object>();
		this.mPwdFlag = false;
	}
	
	@Override
	protected Boolean doInBackground(String... params){
		try {
			mCursor = mSqliteUtil.query(Constant.TABLE_USER, "*", Constant.ID + "=" + Constant.TABLE_ID_KEY, "", -1, -1);
			if (mCursor.getCount() == 0) {
				return false;
			}
			mCursor.moveToFirst();
			mArrayList.add(new BasicNameValuePair("username", mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_NAME))));
			mArrayList.add(new BasicNameValuePair("password", mCursor.getString(mCursor.getColumnIndex(Constant.TABLE_USER_PASSWORD))));
			mArrayList.add(new BasicNameValuePair("mflag", String.valueOf(mWhich)));//1 nickname   2 signature   3 sex
			mArrayList.add(new BasicNameValuePair("mtext", mText));
			if (mWhich == 4) {
				mArrayList.add(new BasicNameValuePair("oldpwd", params[0]));
			}
			mCursor.close();
			mHttpOperation = new HttpOperation(1, Constant.SERVER_MODIFY_INFO_URL, "", null);
			mHttpOperation.setHttpRequest(mArrayList);
			while(!mHttpOperation.mDoneFlag){}
			mJsonObject = new JSONObject(mHttpOperation.mResult);
			if (mJsonObject.getString(Constant.JSON_RESULT_HEAD) == null || mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("3330")) {
				return false;
			}else if(mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("3331")) {
				if (mWhich == 1) {
					mArrayList4UpdateDb.add(Constant.TABLE_USER_NICKNAME);
					mArrayList4UpdateDb.add(mText);
					if (mSqliteUtil.update(Constant.TABLE_USER, mArrayList4UpdateDb, Constant.ID + "=" + Constant.TABLE_ID_KEY)) {
						return true;
					}else {
						return false;
					}
				}else if (mWhich == 2) {
					mArrayList4UpdateDb.add(Constant.TABLE_USER_SINCON);
					mArrayList4UpdateDb.add(mText);
					if (mSqliteUtil.update(Constant.TABLE_USER, mArrayList4UpdateDb, Constant.ID + "=" + Constant.TABLE_ID_KEY)) {
						return true;
					}else {
						return false;
					}
				}else if (mWhich == 3) {
					mArrayList4UpdateDb.add(Constant.TABLE_USER_SEX);
					mArrayList4UpdateDb.add(mText);
					if (mSqliteUtil.update(Constant.TABLE_USER, mArrayList4UpdateDb, Constant.ID + "=" + Constant.TABLE_ID_KEY)) {
						return true;
					}else {
						return false;
					}
				}else if(mWhich == 4){
					mArrayList4UpdateDb.add(Constant.TABLE_USER_PASSWORD);
					mArrayList4UpdateDb.add(mText);
					if (mSqliteUtil.update(Constant.TABLE_USER, mArrayList4UpdateDb, Constant.ID + "=" + Constant.TABLE_ID_KEY)) {
						return true;
					}else {
						return false;
					}
				}else {
					return false;
				}
			}else if(mJsonObject.getString(Constant.JSON_RESULT_HEAD).equals("3332")) {
				mPwdFlag = true;
				return false;
			}else {
				return false;
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}
	}
	
	@Override
	protected void onPostExecute(Boolean result){
		if (result) {
			Toast.makeText(mContext, mContext.getResources().getString(R.string.msg_modify_success), Toast.LENGTH_SHORT).show();
		}else {
			if (mPwdFlag) {
				Toast.makeText(mContext, mContext.getResources().getString(R.string.msg_modify_old_pwd_error), Toast.LENGTH_SHORT).show();
			}else {
				Toast.makeText(mContext, mContext.getResources().getString(R.string.msg_modify_failed), Toast.LENGTH_SHORT).show();
			}
		}
	}
}
